FROM node:16 AS build
WORKDIR /usr/src/app
COPY . .
ENV CYPRESS_INSTALL_BINARY=0
RUN npm ci
RUN npm run build

FROM nginx:alpine
COPY --from=build /usr/src/app/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf
