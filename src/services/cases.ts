import axios from 'axios';
import * as types from '../types';

const getAll = async (): Promise<Array<types.Case>> => {
  const response = await axios.get<Array<types.Case>>('/case');
  return response.data;
};

const getById = async (id: string): Promise<types.Case> => {
  const response = await axios.get<types.Case>(`/case/${id}`);
  return response.data;
};

const getFileCategories = async (): Promise<Array<string>> => {
  const response = await axios.get<Array<string>>('/case/file_categories');
  return response.data;
};

const getKeywords = async (): Promise<Array<string>> => {
  const response = await axios.get<Array<string>>('/case/keywords');
  return response.data;
};

const getPedagogicFields = async (): Promise<Array<string>> => {
  const response = await axios.get<Array<string>>('/case/pedagogic_fields');
  return response.data;
};

const getProblemAreas = async (): Promise<Array<string>> => {
  const response = await axios.get<Array<string>>('/case/problem_areas');
  return response.data;
};

const createCase = async (newCase: types.NewCase): Promise<types.Case> => {
  const response = await axios.post<types.Case>('/case', newCase);
  return response.data;
};

const deleteCase = async (caseId: string): Promise<void> => {
  await axios.delete(`/case/${caseId}`);
};

const searchCase = async (
  q: string,
  offset = 0,
  limit = 10,
): Promise<types.CaseSearchResults> => {
  const response = await axios.post<types.CaseSearchResults>(
    '/case/search/simple',
    {
      q,
      offset,
      limit,
    },
  );
  return response.data;
};

const searchCaseAdvanced = async (
  term: string,
  fileCategories: Array<string>,
  pedagogicFields: Array<string>,
  problemAreas: Array<string>,
): Promise<types.CaseSearchResults> => {
  const response = await axios.post<types.CaseSearchResults>(
    '/case/search/advanced',
    { term, fileCategories, pedagogicFields, problemAreas },
  );
  return response.data;
};

const updateCase = async (
  caseId: string,
  updatedCase: types.NewCase,
): Promise<types.Case> => {
  const response = await axios.put<types.Case>(`/case/${caseId}`, updatedCase);
  return response.data;
};

/**
 * Uploads provided file to the specified case
 * @param caseId
 * @param folder - sub-folder inside the case folder
 * @param file
 * @param progressCallback - optional callback function, useful for display upload progress
 */
const addFileToCase = async (
  caseId: string,
  folder: string,
  file: File,
  progressCallback?: (progressEvent: ProgressEvent) => void,
): Promise<Record<string, unknown>> => {
  const formData = new FormData();
  formData.append('file', file);
  const config = {
    headers: {
      'content-type': 'multipart/form-data',
    },
    onUploadProgress: progressCallback ? progressCallback : undefined,
  };
  const response = await axios.post<Record<string, unknown>>(
    `/case/${caseId}/file/${folder}`,
    formData,
    config,
  );
  return response.data;
};

const deleteFileFromCase = async (
  caseId: string,
  name: string,
): Promise<void> => {
  await axios.delete(`/case/${caseId}/file/${name}`);
};

const getFilesForCase = async (
  caseId: string,
): Promise<Array<types.CaseFile>> => {
  const response = await axios.get<Array<types.CaseFile>>(
    `/case/${caseId}/file`,
  );
  return response.data;
};

export default {
  getAll,
  getById,
  getFileCategories,
  getKeywords,
  getPedagogicFields,
  getProblemAreas,
  createCase,
  deleteCase,
  searchCase,
  searchCaseAdvanced,
  updateCase,
  addFileToCase,
  deleteFileFromCase,
  getFilesForCase,
};
