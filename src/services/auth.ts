import axios from 'axios';
import { mutate } from 'swr';
import { LoginState } from '../types';

/* eslint-disable @typescript-eslint/no-unsafe-member-access */
// `axios.defaults.headers` are defined as type 'any' in the upstream type definitions and lead to 'unsafe-member-access' errors .
// Related PR: https://github.com/axios/axios/pull/3021 - remove ignore once this is merged or the issue is otherwise resolved

const login = async (
  username: string,
  password: string,
): Promise<LoginState> => {
  const response = await axios.post<LoginState>('/auth/login', {
    username,
    password,
  });

  axios.defaults.headers.common[
    'Authorization'
  ] = `bearer ${response.data.token}`;
  return response.data;
};

const logout = async (): Promise<Record<string, unknown>> => {
  const response = await axios.get<Record<string, unknown>>('/auth/logout');
  delete axios.defaults.headers.common['Authorization'];
  return response.data;
};

const refreshToken = async (): Promise<LoginState> => {
  const response = await axios.get<LoginState>('/auth/verify_session');
  if (response.status === 204)
    throw 'Missing refresh token. User is not logged in.';

  axios.defaults.headers.common[
    'Authorization'
  ] = `bearer ${response.data.token}`;
  await mutate('/api/case');
  return response.data;
};

export default {
  login,
  logout,
  refreshToken,
};
