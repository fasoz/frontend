import axios from 'axios';
import * as types from '../types';

const createUser = async (
  newUser: types.NewOrEditedUser,
): Promise<types.User> => {
  const response = await axios.post<types.User>('/user', newUser);
  return response.data;
};

const deleteUser = async (userId: string): Promise<void> => {
  await axios.delete(`/user/${userId}`);
};

const editUser = async (
  userId: string,
  editedUser: types.NewOrEditedUser,
): Promise<types.User> => {
  const response = await axios.put<types.User>(`/user/${userId}`, editedUser);
  return response.data;
};

const forgotPassword = async (
  email: string,
): Promise<Record<string, unknown>> => {
  const response = await axios.post<Record<string, unknown>>(
    '/user/forgot_password',
    { email },
  );
  return response.data;
};

const getAll = async (): Promise<Array<types.User>> => {
  const response = await axios.get<Array<types.User>>('/user');
  return response.data;
};

const getByUserId = async (userId: string): Promise<types.User> => {
  const response = await axios.get<types.User>(`/user/${userId}`);
  return response.data;
};

const resetPassword = async (
  code: string,
  password: string,
): Promise<Record<string, unknown>> => {
  try {
    const response = await axios.post<Record<string, unknown>>(
      '/user/reset_password',
      {
        code,
        password,
      },
    );
    return response.data;
  } catch (error) {
    if (
      axios.isAxiosError(error) &&
      error.response?.status === 400 &&
      typeof error.response.data.error === 'string'
    )
      throw Error(error.response.data.error as string);
    else throw Error('Ein unbekannter Fehler ist aufgetreten.');
  }
};

const verifyEmailAddress = async (verificationCode: string): Promise<void> => {
  try {
    await axios.post(`/user/verify_email/${verificationCode}`);
  } catch (error) {
    if (error.response.status === 404)
      throw Error('Verifizierungscode ungültig.');
    else throw Error('Ein unbekannter Fehler ist aufgetreten.');
  }
};

export default {
  createUser,
  deleteUser,
  editUser,
  forgotPassword,
  getAll,
  getByUserId,
  resetPassword,
  verifyEmailAddress,
};
