import axios from 'axios';

const elasticsearchRebuildIndex = async (): Promise<void> => {
  await axios.post('/admin/elasticsearch/rebuild_index');
};

const elasticsearchStats = async (): Promise<Record<string, number>> => {
  const response = await axios.get<Record<string, number>>(
    '/admin/elasticsearch/stats',
  );
  return response.data;
};

export default {
  elasticsearchRebuildIndex,
  elasticsearchStats,
};
