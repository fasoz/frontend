import axios from 'axios';
import { ContactFormMessage } from '../types';

const contact = async (message: ContactFormMessage): Promise<void> => {
  await axios.post<ContactFormMessage>('/contact', message);
};

export default { contact };
