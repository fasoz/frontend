import React, { useEffect, useState } from 'react';
import { Route, Switch } from 'react-router-dom';
import 'fontsource-roboto';
import {
  createStyles,
  makeStyles,
  Theme,
  ThemeProvider,
} from '@material-ui/core/styles';
import { Container, CssBaseline } from '@material-ui/core';
import axios from 'axios';
import UserContext from './UserContext';
import theme from './theme';
import authService from './services/auth';
import { LoginState } from './types';

import CaseAdvancedSearch from './components/case/AdvancedSearch';
import CaseCreateForm from './components/case/CreateForm';
import CaseEditForm from './components/case/EditForm';
import CaseList from './components/case/List';
import CaseView from './components/case/View';
import ContactForm from './components/ContactForm';
import Footer from './components/layout/Footer';
import Header from './components/layout/Header';
import Homepage from './components/Homepage';
import FAQ from './components/FAQ';
import Impressum from './components/Impressum';
import LoginForm from './components/account/LoginForm';
import MyAccount from './components/account/MyAccount';
import PageNotFound from './components/PageNotFound';
import PrivacyStatement from './components/PrivacyStatement';
import CaseSearch from './components/case/Search';
import SubHeader from './components/layout/SubHeader';
import RegistrationForm from './components/account/RegistrationForm';
import UserContainer from './components/user/Container';
import VerifyEmail from './components/account/VerifyEmail';
import ResetPassword from './components/account/ResetPassword';
import ForgotPassword from './components/account/ForgotPassword';
import AdminElasticsearch from './components/admin/Elasticsearch';
import EditMyAccount from './components/account/EditMyAccount';
import EthicsCodex from './components/EthicsCodex';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    content: {
      marginTop: theme.spacing(2),
    },
  }),
);

const App = (): JSX.Element => {
  axios.defaults.baseURL = '/api/';

  const classes = useStyles('');

  const [user, setUser] = useState<LoginState | null | undefined>(undefined);

  const refreshAccessToken = () => {
    authService
      .refreshToken()
      .then((response) => setUser(response))
      .catch(() => setUser(null));
  };

  useEffect(() => {
    refreshAccessToken();
    setInterval(refreshAccessToken, 10 * 60 * 1000);
  }, []);

  return (
    <UserContext.Provider value={{ user, setUser }}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Header />
        <SubHeader />
        <Container className={classes.content} component="main">
          <Switch>
            <Route exact path="/">
              <Homepage />
            </Route>
            <Route path="/admin/elasticsearch">
              <AdminElasticsearch />
            </Route>
            <Route path="/anmelden">
              <LoginForm />
            </Route>
            <Route path="/datenschutzerklärung">
              <PrivacyStatement />
            </Route>
            <Route path="/email_bestätigen/:code">
              <VerifyEmail />
            </Route>
            <Route path="/erweiterte_suche">
              <CaseAdvancedSearch />
            </Route>
            <Route path="/ethikkodex">
              <EthicsCodex />
            </Route>
            <Route path="/fall/neu">
              <CaseCreateForm />
            </Route>
            <Route path="/fall/:caseId/bearbeiten">
              <CaseEditForm />
            </Route>
            <Route path="/fall/:caseId">
              <CaseView />
            </Route>
            <Route path="/fallverzeichnis">
              <CaseList />
            </Route>
            <Route path="/faq">
              <FAQ />
            </Route>
            <Route path="/impressum">
              <Impressum />
            </Route>
            <Route path="/kontakt">
              <ContactForm />
            </Route>
            <Route path="/passwort_vergessen">
              <ForgotPassword />
            </Route>
            <Route path="/passwort_zurücksetzen/:code">
              <ResetPassword />
            </Route>
            <Route path="/registrieren">
              <RegistrationForm />
            </Route>
            <Route path="/suche/:q">
              <CaseSearch />
            </Route>
            <Route path="/meinkonto/bearbeiten">
              <EditMyAccount />
            </Route>
            <Route path="/meinkonto">
              <MyAccount />
            </Route>
            <Route path="/zugangsverwaltung">
              <UserContainer />
            </Route>
            <Route path="*">
              <PageNotFound />
            </Route>
          </Switch>
        </Container>
        <Footer />
      </ThemeProvider>
    </UserContext.Provider>
  );
};

export default App;
