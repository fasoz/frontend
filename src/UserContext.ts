import React from 'react';
import { LoginState } from './types';

interface UserContextValue {
  // user === undefined means it is unknown whether the user is logged in
  // user === null means user is not logged in
  // user instanceof LoginState means user is logged in
  user: LoginState | null | undefined;
  setUser: React.Dispatch<React.SetStateAction<LoginState | null | undefined>>;
}

const initialValue: UserContextValue = {
  user: undefined,
  setUser: (): void => {
    throw new Error('setUser must be overridden');
  },
};

const UserContext = React.createContext<UserContextValue>(initialValue);

export default UserContext;
