import { createTheme, responsiveFontSizes } from '@material-ui/core/styles';
import { blue, blueGrey, orange, purple } from '@material-ui/core/colors';

const theme = createTheme({
  palette: {
    type: 'light',
    primary: blue,
    secondary: {
      dark: '#bd0e24',
      main: '#CC1F2F',
      light: '#fa3b3f',
    },
    warning: orange,
    info: purple,
    background: {
      default: 'white',
      paper: blueGrey['50'],
    },
  },
  shape: {
    borderRadius: 5,
  },
  typography: {
    fontFamily: 'Roboto, Helvetica Neue, Arial, sans-serif',
    body1: {
      lineHeight: 1.7,
    },
    body2: {
      lineHeight: 1.7,
    },
  },
});

export default responsiveFontSizes(theme);
