import React from 'react';
import { Link, Typography } from '@material-ui/core/';
import MailIcon from '@material-ui/icons/Mail';
import { Container } from '@material-ui/core';

interface MailLinkProps {
  address: string;
}

const MailLink: React.FC<MailLinkProps> = ({ address }) => (
  <Link
    style={{
      alignItems: 'center',
      display: 'inline-flex',
    }}
    href={`mailto:${address}`}
  >
    <MailIcon />
    {address}
  </Link>
);

const Impressum: React.FC = () => {
  return (
    <Container maxWidth="sm">
      <Typography variant="h4" gutterBottom>
        Impressum
      </Typography>

      <Typography variant="h6">
        Angaben zum gemeinsamen Verantwortlichen
      </Typography>
      <Typography variant="body2" gutterBottom>
        Präsident der Frankfurt University of Applied Sciences Prof. Dr. Frank
        E.P. Dievernich
        <br />
        Nibelungenplatz 1<br />
        60318 Frankfurt am Main
        <br />
        Tel.: 069 1533 2415
        <br />
        <MailLink address="praesident@fra-uas.de" />
      </Typography>

      <Typography variant="h6">
        Angaben zur Person des Datenschutzbeauftragten
      </Typography>
      <Typography variant="body2" gutterBottom>
        Alexander Becker
        <br />
        Nibelungenplatz 1<br />
        60318 Frankfurt am Main
        <br />
        Tel.: 069 1533 3020
        <br />
        <MailLink address="dsb@fra-uas.de" />
      </Typography>

      <Typography variant="h6">Technisch verantwortliche Person</Typography>
      <Typography variant="body2" gutterBottom>
        Marcus Legendre
        <br />
        Frankfurt University of Applied Sciences
        <br />
        Nibelungenplatz 1<br />
        60318 Frankfurt am Main
        <br />
        <MailLink address="legendre@fb2.fra-uas.de" />
      </Typography>
    </Container>
  );
};

export default Impressum;
