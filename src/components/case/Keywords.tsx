import React from 'react';
import { useHistory } from 'react-router-dom';
import useSWR from 'swr';
import { Chip, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Alert, AlertTitle } from '@material-ui/lab';
import casesService from '../../services/cases';
import LoadingIndicator from '../layout/LoadingIndicator';

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(2),
  },
  chip: {
    margin: theme.spacing(0.2),
  },
}));

const CaseKeywords = (): JSX.Element => {
  const classes = useStyles();

  const { data, error } = useSWR('/api/keywords', casesService.getKeywords, {
    refreshInterval: 0,
    revalidateOnFocus: false,
  });
  const history = useHistory();

  if (error) {
    return (
      <Alert severity="error">
        <AlertTitle>Fehler beim Laden der Schlagwörter.</AlertTitle>
        {error.toString()}
      </Alert>
    );
  }

  if (!data) return <LoadingIndicator text="Schlagwörter werden geladen" />;

  if (data.length === 0) return <></>;

  return (
    <div className={classes.root}>
      <Typography gutterBottom variant="h4">
        Schlagwörter
      </Typography>
      {data.map((keyword: string) => (
        <Chip
          key={keyword}
          clickable
          className={classes.chip}
          label={keyword}
          onClick={() => {
            history.push(`/suche/${keyword}`);
            window.scrollTo(0, 0);
          }}
          size="small"
          variant="outlined"
        />
      ))}
    </div>
  );
};
export default CaseKeywords;
