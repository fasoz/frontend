import React, { useContext, useEffect, useState } from 'react';
import * as yup from 'yup';
import { useHistory } from 'react-router-dom';

import { Button, Checkbox, TextField, TextFieldProps } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Alert, AlertTitle, Autocomplete } from '@material-ui/lab';
import SaveIcon from '@material-ui/icons/Save';
import CancelIcon from '@material-ui/icons/Cancel';

import {
  Field,
  FieldAttributes,
  Form,
  Formik,
  FormikHelpers,
  useField,
} from 'formik';

import { AlertMessage, Case, NewCase, UserRole } from '../../types';
import UserContext from '../../UserContext';
import casesService from '../../services/cases';
import { roleToTranslatedString } from '../../utils/roleTranslation';

const useStyles = makeStyles((theme) => ({
  formActions: {
    textAlign: 'right',
  },
  button: {
    marginLeft: theme.spacing(1),
  },
}));

const CustomTextField: React.FC<
  TextFieldProps & FieldAttributes<Record<string, unknown>>
> = ({ label, multiline, ...props }) => {
  const [field, meta] = useField<Record<string, unknown>>(props);
  const errorText = meta.error && meta.touched ? meta.error : '';
  return (
    <TextField
      {...field}
      fullWidth
      type={props.type}
      label={label}
      multiline={multiline}
      error={Boolean(errorText)}
      helperText={errorText}
    />
  );
};

const validationSchema = yup.object({
  title: yup
    .string()
    .required('Pflichtfeld')
    .min(3, 'Titel muss mindestens 3 Zeichen lang sein.'),
  description: yup.string(),
  keywords: yup.array().of(yup.string()),
  participants: yup.string(),
  pedagogicFields: yup.array().of(yup.string()),
  problemAreas: yup.array().of(yup.string()),
  fileAccessAllowedRoles: yup
    .array()
    .of(yup.mixed().oneOf(Object.values(UserRole))),
});

interface CaseBaseFormProps {
  handleSaveCase: (caseToSave: NewCase) => Promise<Case>;
  initialValues: NewCase | Case;
}

const CaseBaseForm: React.FC<CaseBaseFormProps> = ({
  handleSaveCase,
  initialValues,
}) => {
  const [alertMessage, setAlertMessage] = useState<AlertMessage>();
  const [pedagogicFieldsOptions, setPedagogicFieldsOptions] = useState<
    Array<string>
  >([]);
  const [fileCategoriesOptions, setFileCategoriesOptions] = useState<
    Array<string>
  >([]);
  const [keywordOptions, setKeywordOptions] = useState<Array<string>>([]);
  const [problemAreaOptions, setProblemAreaOptions] = useState<Array<string>>(
    [],
  );
  const classes = useStyles();
  const history = useHistory();
  const { user } = useContext(UserContext);

  useEffect(() => {
    void casesService
      .getKeywords()
      .then((response) => setKeywordOptions(response));
    void casesService
      .getFileCategories()
      .then((response) => setFileCategoriesOptions(response));
    void casesService
      .getPedagogicFields()
      .then((response) => setPedagogicFieldsOptions(response));
    void casesService
      .getProblemAreas()
      .then((response) => setProblemAreaOptions(response));
  }, []);

  const handleSubmit = async (
    caseToSubmit: NewCase,
    { setSubmitting }: FormikHelpers<NewCase>,
  ) => {
    setSubmitting(true);

    caseToSubmit = {
      title: caseToSubmit.title.trim(),
      description: caseToSubmit.description,
      fileCategories: caseToSubmit.fileCategories.map((s) => s.trim()),
      keywords: caseToSubmit.keywords.map((s) => s.trim()),
      participants: caseToSubmit.participants.trim(),
      pedagogicFields: caseToSubmit.pedagogicFields.map((s) => s.trim()),
      problemAreas: caseToSubmit.problemAreas.map((s) => s.trim()),
      published: caseToSubmit.published,
      fileAccessAllowedRoles: caseToSubmit.fileAccessAllowedRoles,
    };

    try {
      const savedCase = await handleSaveCase(caseToSubmit);
      history.push(`/fall/${savedCase.id}`);
    } catch (error) {
      setAlertMessage({
        severity: 'error',
        message: error.response.data.error || error.response.data,
      });
    }

    setSubmitting(false);
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ isSubmitting, values, setFieldValue }) => (
          <Form id="addCaseForm">
            <Field as={CustomTextField} name="title" label="Titel" />
            <Field
              as={CustomTextField}
              name="description"
              label="Kurzbeschreibung"
              multiline
            />
            <Autocomplete
              id="fileCategoriesField"
              multiple
              filterSelectedOptions
              freeSolo
              size="small"
              value={values.fileCategories}
              onChange={(event, value) => {
                setFieldValue('fileCategories', value);
              }}
              renderInput={(params) => (
                <Field
                  {...params}
                  component={TextField}
                  name="fileCategories"
                  label="Materialarten"
                />
              )}
              options={fileCategoriesOptions}
            />
            <Autocomplete
              id="keywordsField"
              multiple
              filterSelectedOptions
              freeSolo
              size="small"
              value={values.keywords}
              onChange={(event, value) => {
                setFieldValue('keywords', value);
              }}
              renderInput={(params) => (
                <Field
                  {...params}
                  component={TextField}
                  name="keywords"
                  label="Schlagwörter"
                />
              )}
              options={keywordOptions}
            />
            <Field
              as={CustomTextField}
              name="participants"
              label="Beteiligte"
              multiline
            />
            <Autocomplete
              id="pedagogicFieldsField"
              multiple
              filterSelectedOptions
              freeSolo
              size="small"
              value={values.pedagogicFields}
              onChange={(event, value) => {
                setFieldValue('pedagogicFields', value);
              }}
              renderInput={(params) => (
                <Field
                  {...params}
                  component={TextField}
                  name="pedagogicFields"
                  label="Pädagogische Handlungsfelder"
                />
              )}
              options={pedagogicFieldsOptions}
            />
            <Autocomplete
              id="problemAreasField"
              multiple
              filterSelectedOptions
              freeSolo
              size="small"
              value={values.problemAreas}
              onChange={(event, value) => {
                setFieldValue('problemAreas', value);
              }}
              renderInput={(params) => (
                <Field
                  {...params}
                  component={TextField}
                  name="problemAreas"
                  label="Institution und Arbeitsfeld"
                />
              )}
              options={problemAreaOptions}
            />
            <label>
              Veröffentlicht:{' '}
              <Field
                id="published"
                name="published"
                type="checkbox"
                as={Checkbox}
                disabled={!user?.roles.includes('Editor')}
              />
            </label>
            <br />
            Dateizugriff für folgende Rollen gestatten:
            {Object.values(UserRole).map((role) => (
              <React.Fragment key={role}>
                {' '}
                <label>
                  <Field
                    name="fileAccessAllowedRoles"
                    type="checkbox"
                    value={role}
                    as={Checkbox}
                  />
                  {roleToTranslatedString(role)}
                </label>
              </React.Fragment>
            ))}
            <div className={classes.formActions}>
              <Button
                className={classes.button}
                onClick={() => history.goBack()}
                startIcon={<CancelIcon />}
                variant="contained"
              >
                Abbrechen
              </Button>
              <Button
                className={classes.button}
                color="secondary"
                disabled={isSubmitting}
                id="buttonSaveCase"
                startIcon={<SaveIcon />}
                type="submit"
                variant="contained"
              >
                Speichern
              </Button>
            </div>
          </Form>
        )}
      </Formik>

      {alertMessage && (
        <Alert severity={alertMessage.severity}>
          {alertMessage.title && <AlertTitle>{alertMessage.title}</AlertTitle>}
          {alertMessage.message}
        </Alert>
      )}
    </>
  );
};

export default CaseBaseForm;
