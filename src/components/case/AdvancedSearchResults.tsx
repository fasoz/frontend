import React from 'react';
import { Container, Typography } from '@material-ui/core';
import { CaseSearchHit } from '../../types';
import CaseCard from './Card';

interface CaseAdvancedSearchResultsProps {
  results: Array<CaseSearchHit>;
}

const CaseAdvancedSearchResults: React.FC<CaseAdvancedSearchResultsProps> = ({
  results,
}) => {
  return (
    <Container maxWidth="md">
      <Typography align="center" variant="h4">
        Suchergebnisse
      </Typography>

      <Typography gutterBottom variant="subtitle1">
        {results.length} {results.length === 1 ? 'Ergebnis' : 'Ergebnisse'}
      </Typography>

      {results
        .map((result) => ({ ...result.case }))
        .map((thisCase) => (
          <CaseCard key={thisCase.id} thisCase={thisCase} />
        ))}
    </Container>
  );
};

export default CaseAdvancedSearchResults;
