import React from 'react';
import { useSWRConfig } from 'swr';

import { Container, Typography } from '@material-ui/core';
import { Alert } from '@material-ui/lab';

import CaseBaseForm from './BaseForm';

import casesService from '../../services/cases';
import { Case, NewCase, UserRole } from '../../types';

const CaseCreateForm: React.FC = () => {
  const { mutate } = useSWRConfig();

  const initialValues = {
    title: '',
    description: '',
    fileCategories: [],
    keywords: [],
    participants: '',
    pedagogicFields: [],
    problemAreas: [],
    published: false,
    fileAccessAllowedRoles: Object.values(UserRole),
  };

  const handleSaveCase = async (newCase: NewCase): Promise<Case> => {
    const savedCase = await casesService.createCase(newCase);
    await mutate('/api/case');
    return savedCase;
  };

  return (
    <Container maxWidth="md">
      <Typography variant="h4">Neuen Fall anlegen</Typography>
      <CaseBaseForm
        handleSaveCase={handleSaveCase}
        initialValues={initialValues}
      />

      <Typography variant="h4">Fallmaterial</Typography>
      <Alert severity="info">
        Das Hochladen von Dateien ist erst nach Anlegen des Falls möglich.
      </Alert>
    </Container>
  );
};

export default CaseCreateForm;
