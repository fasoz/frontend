import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { Button, Container, Typography } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import casesService from '../../services/cases';
import { AlertMessage, CaseSearchHit, CaseSearchNumHits } from '../../types';
import CaseCard from './Card';
import LoadingIndicator from '../layout/LoadingIndicator';

const CaseSearch: React.FC = () => {
  const { q } = useParams<{ q: string }>();
  const [offset, setOffset] = useState(0);
  const limit = 10;
  const [numHits, setNumHits] = useState<CaseSearchNumHits>({
    value: 0,
    relation: 'eq',
  });
  const [results, setResults] = useState<Array<CaseSearchHit>>([]);
  const [error, setError] = useState<AlertMessage>();

  useEffect(() => {
    setOffset(0);
    casesService
      .searchCase(q, 0, limit)
      .then((data) => {
        setNumHits(data.numHits);
        setResults(data.hits);
      })
      .catch((error) =>
        setError({
          severity: 'error',
          title: 'Fehler bei der Suche',
          message: error.message,
        }),
      );
  }, [q]);

  const hasMoreResults = (): boolean => offset + limit < numHits.value;

  const handleLoadMore = () => {
    const newOffset = offset + limit;
    setOffset(newOffset);
    casesService
      .searchCase(q, newOffset, limit)
      .then((data) => setResults(results.concat(data.hits)))
      .catch((error) =>
        setError({
          severity: 'error',
          title: 'Fehler bei der Suche',
          message: error.message,
        }),
      );
  };

  if (!results) return <LoadingIndicator text="Suchanfrage wird verarbeitet" />;

  return (
    <Container maxWidth="md" component="section" role="feed">
      <Typography variant="h4">Suchergebnisse für &bdquo;{q}&ldquo;</Typography>

      {error && (
        <Alert severity="error">
          <AlertTitle>Fehler beim Durchsuchen der Datenbank</AlertTitle>
          {error}
        </Alert>
      )}

      <Typography variant="subtitle1">
        {numHits.relation === 'gte' && 'Mindestens '}
        {numHits.value} {numHits.value === 1 ? 'Ergebnis' : 'Ergebnisse'}
      </Typography>

      {results
        .map((result) => ({ ...result.case, ...result.highlight }))
        .map((thisCase) => (
          <CaseCard key={thisCase.id} thisCase={thisCase} />
        ))}

      {hasMoreResults() && (
        <div style={{ textAlign: 'center' }}>
          <Button color="secondary" onClick={handleLoadMore} variant="outlined">
            Mehr Ergebnisse laden
          </Button>
        </div>
      )}
    </Container>
  );
};

export default CaseSearch;
