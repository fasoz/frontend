import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { Case } from '../../types';
import {
  Card,
  CardActionArea,
  CardContent,
  Chip,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(2),
    '& em': {
      fontWeight: 'bold',
      textDecoration: 'underline',
      textDecorationColor: theme.palette.warning.main,
    },
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  title: {
    color: theme.palette.primary.dark,
  },
  isUnpublishedChip: {
    backgroundColor: theme.palette.warning.light,
    color: theme.palette.getContrastText(theme.palette.warning.light),
    marginLeft: '0.25rem',
  },
  description: {
    marginTop: theme.spacing(1),
    lineHeight: 1.6,
  },
}));

interface CaseCardProps {
  thisCase: Case;
}

const CaseCard: React.FC<CaseCardProps> = ({ thisCase }) => {
  const classes = useStyles();

  return (
    <Card
      className={`${classes.root} caseCard`}
      component="article"
      aria-labelledby={`${thisCase.id}-title`}
      aria-describedby={`${thisCase.id}-description`}
    >
      <CardActionArea
        component={RouterLink}
        onClick={() => window.scrollTo(0, 0)}
        to={`/fall/${thisCase.id}`}
      >
        <CardContent>
          <div className={classes.header}>
            <Typography
              className={classes.title}
              dangerouslySetInnerHTML={{ __html: thisCase.title }}
              id={`${thisCase.id}-title`}
              variant="h5"
            />

            {!thisCase.published && (
              <Chip
                className={classes.isUnpublishedChip}
                color="secondary"
                label="Unveröffentlichter Entwurf"
                icon={<VisibilityOffIcon />}
                size="small"
              />
            )}
          </div>

          <Table size="small">
            <TableBody>
              {thisCase.fileCategories.length > 0 && (
                <TableRow>
                  <TableCell width="50%">Materialarten</TableCell>
                  <TableCell
                    width="50%"
                    dangerouslySetInnerHTML={{
                      __html: thisCase.fileCategories.join(', '),
                    }}
                  />
                </TableRow>
              )}

              {thisCase.pedagogicFields.length > 0 && (
                <TableRow>
                  <TableCell width="50%">Handlungsfeld</TableCell>
                  <TableCell
                    width="50%"
                    dangerouslySetInnerHTML={{
                      __html: thisCase.pedagogicFields.join(', '),
                    }}
                  />
                </TableRow>
              )}

              {thisCase.problemAreas.length > 0 && (
                <TableRow>
                  <TableCell width="50%">Institution und Arbeitsfeld</TableCell>
                  <TableCell
                    width="50%"
                    dangerouslySetInnerHTML={{
                      __html: thisCase.problemAreas.join(', '),
                    }}
                  />
                </TableRow>
              )}
            </TableBody>
          </Table>

          <Typography
            className={classes.description}
            dangerouslySetInnerHTML={{
              __html: Array.isArray(thisCase.description)
                ? thisCase.description.join(' ... ')
                : thisCase.description.length < 500
                ? thisCase.description
                : thisCase.description.substr(
                    0,
                    thisCase.description.indexOf(' ', 500),
                  ) + '...',
            }}
            gutterBottom
            id={`${thisCase.id}-description`}
            variant="body2"
          />
        </CardContent>
      </CardActionArea>
    </Card>
  );
};

export default CaseCard;
