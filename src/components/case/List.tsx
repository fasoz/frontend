import React, { useContext } from 'react';
import useSWR from 'swr';

import { Fab, Hidden, Typography } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import { makeStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';

import CaseCard from './Card';
import CaseKeywords from './Keywords';
import CasePedagogicFields from './PedagogicFields';
import LoadingIndicator from '../layout/LoadingIndicator';

import UserContext from '../../UserContext';
import casesService from '../../services/cases';

import * as types from '../../types';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    [theme.breakpoints.down('sm')]: {
      flexFlow: 'column',
    },
  },
  actionButton: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
  cases: {
    position: 'relative',
    [theme.breakpoints.up('md')]: {
      marginRight: theme.spacing(1),
      width: '75%',
    },
    [theme.breakpoints.down('sm')]: {
      width: '100%',
    },
  },
  keywords: {
    marginLeft: theme.spacing(1),
    width: '25%',
  },
}));

const CaseList: React.FC = () => {
  const classes = useStyles();
  const { user } = useContext(UserContext);
  const { data, error } = useSWR('/api/case', casesService.getAll);

  if (!data) return <LoadingIndicator text="Fälle werden geladen" />;

  if (error) {
    return (
      <Alert severity="error">
        <AlertTitle>Fehler beim Laden der Fälle.</AlertTitle>
        {error.toString()}
      </Alert>
    );
  }

  return (
    <div className={classes.root}>
      <div className={classes.cases}>
        <Typography gutterBottom variant="h4">
          Fallverzeichnis
        </Typography>

        {user && user.roles?.includes('Researcher') && (
          <Fab
            className={classes.actionButton}
            id="addCaseButton"
            color="secondary"
            variant="extended"
            href="/fall/neu"
          >
            <AddIcon /> Fall hinzufügen
          </Fab>
        )}

        {data.length === 0
          ? 'Keine Fälle in der Datenbank.'
          : data.map((thisCase: types.Case) => (
              <CaseCard key={thisCase.id} thisCase={thisCase} />
            ))}
      </div>

      <div className={classes.keywords}>
        <Hidden smDown>
          <CasePedagogicFields />
          <CaseKeywords />
        </Hidden>
      </div>
    </div>
  );
};

export default CaseList;
