import { Container, Divider, Typography } from '@material-ui/core';
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: theme.spacing(3),
    marginBottom: theme.spacing(3),
  },
  title: {
    fontFamily: 'Roboto Condensed',
    fontSize: '1.2rem',
    fontVariant: 'small-caps',
  },
  content: {
    fontSize: '1.5rem',
    textAlign: 'center',
    whiteSpace: 'pre-wrap',
  },
}));

interface MetaDataBoxProps {
  title: string;
}

const DataBox: React.FC<MetaDataBoxProps> = ({
  title,
  children,
}): JSX.Element => {
  const classes = useStyles();

  return (
    <>
      <Container
        className={classes.container}
        maxWidth="xl"
        component="section"
      >
        <Typography
          align="center"
          className={classes.title}
          gutterBottom
          variant="h6"
        >
          {title}
        </Typography>
        <div className={classes.content}>{children}</div>
      </Container>
      <Divider />
    </>
  );
};

export default DataBox;
