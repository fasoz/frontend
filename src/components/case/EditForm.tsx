import React, { useContext } from 'react';
import { useParams } from 'react-router-dom';
import useSWR, { useSWRConfig } from 'swr';

import { Container, Typography } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';

import CaseBaseForm from './BaseForm';
import EditCaseFiles from './files/EditCaseFiles';
import LoadingIndicator from '../layout/LoadingIndicator';

import casesService from '../../services/cases';
import { NewCase, Case } from '../../types';
import UserContext from '../../UserContext';

const CaseEditForm: React.FC = () => {
  const { caseId } = useParams<{ caseId: string }>();
  const { user } = useContext(UserContext);
  const { data, error } = useSWR<Case>(
    user ? `/api/case/${caseId}` : null,
    () => casesService.getById(caseId),
  );
  const { mutate } = useSWRConfig();

  if (error) {
    return (
      <Container maxWidth="sm">
        <Alert severity="error">
          <AlertTitle>Fehler beim Laden des Falls.</AlertTitle>
          {error.toString()}
        </Alert>
      </Container>
    );
  }

  if (!data) return <LoadingIndicator text="Fall wird geladen" />;

  const handleSaveCase = async (updatedCase: NewCase): Promise<Case> => {
    const savedCase = await casesService.updateCase(caseId, updatedCase);
    await mutate(
      `/api/case`,
      (cases: Array<Case>) =>
        cases && cases.map((c) => (c.id === caseId ? savedCase : c)),
    );
    await mutate(`/api/case/${caseId}`, savedCase);
    return savedCase;
  };

  return (
    <Container maxWidth="md">
      <Typography variant="h4">Metadaten bearbeiten</Typography>
      <CaseBaseForm handleSaveCase={handleSaveCase} initialValues={data} />

      <Typography variant="h4">Fallmaterial</Typography>
      <EditCaseFiles caseId={caseId} />
    </Container>
  );
};

export default CaseEditForm;
