import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import useSWR from 'swr';
import { makeStyles } from '@material-ui/core/styles';
import { List, ListItem, Typography } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import casesService from '../../services/cases';
import LoadingIndicator from '../layout/LoadingIndicator';

const useStyles = makeStyles((theme) => ({
  root: {
    marginBottom: theme.spacing(2),
  },
  chip: {
    margin: theme.spacing(0.2),
  },
}));

const CasePedagogicFields = (): JSX.Element => {
  const classes = useStyles();

  const { data, error } = useSWR(
    '/api/pedagogic_fields',
    casesService.getPedagogicFields,
    {
      refreshInterval: 0,
      revalidateOnFocus: false,
    },
  );

  if (error) {
    return (
      <Alert severity="error">
        <AlertTitle>Fehler beim Laden der Schlagwörter.</AlertTitle>
        {error.toString()}
      </Alert>
    );
  }

  if (!data) return <LoadingIndicator text="Handlungsfelder werden geladen" />;

  if (data.length === 0) return <></>;

  return (
    <div className={classes.root}>
      <Typography variant="h4">Handlungsfelder</Typography>
      <List dense>
        {data.map((keyword: string) => (
          <ListItem
            button
            component={RouterLink}
            key={keyword}
            onClick={() => window.scrollTo(0, 0)}
            to={`/suche/${keyword}`}
          >
            {keyword}
          </ListItem>
        ))}
      </List>
    </div>
  );
};
export default CasePedagogicFields;
