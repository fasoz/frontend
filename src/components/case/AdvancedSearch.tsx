import React, { useState } from 'react';
import * as yup from 'yup';

import {
  Button,
  Checkbox,
  Container,
  TextField,
  Typography,
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Alert, AlertTitle, Autocomplete } from '@material-ui/lab';
import CheckBoxOutlineBlankIcon from '@material-ui/icons/CheckBoxOutlineBlank';

import CheckBoxIcon from '@material-ui/icons/CheckBox';
import useSWR from 'swr';
import { useFormik } from 'formik';

import CaseAdvancedSearchResults from './AdvancedSearchResults';

import casesService from '../../services/cases';
import { AlertMessage, CaseSearchHit } from '../../types';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      marginBottom: theme.spacing(2),
    },
    textField: {
      marginBottom: theme.spacing(1),
    },
  }),
);

const CaseAdvancedSearch: React.FC = () => {
  const classes = useStyles();
  const [results, setResults] = useState<Array<CaseSearchHit>>();
  const [error, setError] = useState<AlertMessage>();

  const { data: fileCategoriesOptions } = useSWR(
    '/api/case/file_categories',
    casesService.getFileCategories,
  );
  const { data: pedagogicFieldsOptions } = useSWR(
    '/api/case/pedagogic_fields',
    casesService.getPedagogicFields,
  );
  const { data: problemAreasOptions } = useSWR(
    "/api/case/problem_areas'",
    casesService.getProblemAreas,
  );

  const formik = useFormik({
    initialValues: {
      term: '',
      fileCategories: [],
      pedagogicFields: [],
      problemAreas: [],
    },
    validationSchema: yup.object({
      term: yup.string(),
      fileCategories: yup.array().of(yup.string()),
      pedagogicFields: yup.array().of(yup.string()),
      problemAreas: yup.array().of(yup.string()),
    }),
    onSubmit: (values) => {
      void casesService
        .searchCaseAdvanced(
          values.term,
          values.fileCategories,
          values.pedagogicFields,
          values.problemAreas,
        )
        .then((result) => setResults(result.hits))
        .catch((error) =>
          setError({
            severity: 'error',
            title: 'Fehler bei der Suche',
            message: error.message,
          }),
        );
    },
  });

  const autocompleteFields = [
    {
      id: 'fileCategories',
      label: 'Materialarten',
      options: fileCategoriesOptions,
      value: formik.values.fileCategories,
    },
    {
      id: 'pedagogicFields',
      label: 'Pädagogisches Handlungsfeld',
      options: pedagogicFieldsOptions,
      value: formik.values.pedagogicFields,
    },
    {
      id: 'problemAreas',
      label: 'Problemfeld',
      options: problemAreasOptions,
      value: formik.values.problemAreas,
    },
  ];

  return (
    <>
      <Container
        className={classes.container}
        maxWidth="sm"
        component="section"
        role="feed"
      >
        <Typography align="center" variant="h4">
          Erweiterte Suche
        </Typography>

        <form onSubmit={formik.handleSubmit} role="search">
          <TextField
            id="term"
            className={classes.textField}
            name="term"
            label="Suchbegriffe"
            fullWidth
            value={formik.values.term}
            onChange={formik.handleChange}
            error={formik.touched.term && Boolean(formik.errors.term)}
            helperText={formik.touched.term && formik.errors.term}
          />

          {autocompleteFields.map((field) => (
            <Autocomplete
              key={field.id}
              id={field.id}
              className={classes.textField}
              disableCloseOnSelect
              multiple
              size="small"
              value={field.value}
              onChange={(_, value) =>
                void formik.setFieldValue(field.id, value)
              }
              renderInput={(params) => (
                <TextField {...params} name={field.id} label={field.label} />
              )}
              renderOption={(option, { selected }) => (
                <>
                  <Checkbox
                    icon={<CheckBoxOutlineBlankIcon fontSize="small" />}
                    checkedIcon={<CheckBoxIcon fontSize="small" />}
                    checked={selected}
                  />
                  {option}
                </>
              )}
              options={field.options || []}
            />
          ))}

          <Button color="secondary" variant="contained" fullWidth type="submit">
            Suchen
          </Button>
        </form>
      </Container>

      {results && <CaseAdvancedSearchResults results={results} />}

      {error && (
        <Container maxWidth="sm">
          <Alert severity="error">
            <AlertTitle>Fehler beim Durchsuchen der Datenbank</AlertTitle>
            {error}
          </Alert>
        </Container>
      )}
    </>
  );
};

export default CaseAdvancedSearch;
