import React, { useContext } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import useSWR, { useSWRConfig } from 'swr';

import {
  Chip,
  Container,
  Divider,
  Fab,
  Tooltip,
  Typography,
} from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff';

import CaseFiles from './files/Index';
import DataBox from './DataBox';
import LoadingIndicator from '../layout/LoadingIndicator';

import UserContext from '../../UserContext';
import casesService from '../../services/cases';
import { Case } from '../../types';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    case: {
      position: 'relative',
    },
    container: {
      marginTop: theme.spacing(3),
      marginBottom: theme.spacing(3),
    },
    title: {
      fontFamily: 'Exo',
      fontWeight: 'bold',
    },
    chip: {
      margin: '.1rem',
    },
    actions: {
      position: 'absolute',
      top: 0,
      right: 0,
    },
    fab: {
      marginLeft: theme.spacing(1),
    },
  }),
);

const CaseView: React.FC = () => {
  const classes = useStyles();
  const history = useHistory();
  const { caseId } = useParams<{ caseId: string }>();
  const { user } = useContext(UserContext);
  const { data: thisCase, error } = useSWR<Case>(
    user !== undefined ? `/api/case/${caseId}` : null,
    () => casesService.getById(caseId),
    { shouldRetryOnError: false },
  );
  const { mutate } = useSWRConfig();

  if (error?.response?.status === 403) {
    return (
      <Container maxWidth="sm">
        <Alert severity="warning">
          <AlertTitle>Zugriff verweigert.</AlertTitle>
          Sie haben nicht die nötige Berechtigungen um diesen Fall abrufen zu
          können.
        </Alert>
      </Container>
    );
  }

  if (error?.response?.status === 404) {
    return (
      <Container maxWidth="sm">
        <Alert severity="info">
          <AlertTitle>Fall nicht gefunden.</AlertTitle>
          Die Fallkennung (ID) ist ungültig oder der Fall wurde gelöscht.
        </Alert>
      </Container>
    );
  }

  if (error) {
    return (
      <Container maxWidth="sm">
        <Alert severity="error">
          <AlertTitle>Fehler beim Laden des Falls.</AlertTitle>
          {error.toString()}
        </Alert>
      </Container>
    );
  }

  if (!thisCase) return <LoadingIndicator text="Fall wird geladen" />;

  const handleDelete = async () => {
    if (
      !window.confirm(
        `Möchten Sie den Fall "${thisCase.title}" wirklich löschen?`,
      )
    ) {
      return;
    }
    await casesService.deleteCase(caseId);
    await mutate(
      '/api/case',
      (cases: Array<Case>) => cases && cases.filter((c) => c.id !== caseId),
    );
    await mutate(`/api/case/${caseId}`);
  };

  return (
    <div className={classes.case}>
      {!thisCase.published && (
        <Container maxWidth="sm">
          <Alert
            severity="info"
            icon={<VisibilityOffIcon fontSize="inherit" />}
          >
            <AlertTitle>Unveröffentlichter Entwurf</AlertTitle>
            Dieser Fall ist nur für Sie und die Seitenbetreiber sichtbar.
          </Alert>
        </Container>
      )}

      <Container className={classes.container} maxWidth="lg">
        <Typography align="center" variant="subtitle1">
          Fallakte
        </Typography>
        <Typography
          align="center"
          className={classes.title}
          gutterBottom
          variant="h2"
        >
          {thisCase.title}
        </Typography>
      </Container>

      {user && (
        <div className={classes.actions}>
          <Tooltip title="Metadaten bearbeiten">
            <Fab
              className={classes.fab}
              onClick={() => history.push(`/fall/${thisCase.id}/bearbeiten`)}
              id="editCase"
              color="secondary"
            >
              <EditIcon />
            </Fab>
          </Tooltip>
          <Tooltip title="Fall löschen">
            <Fab
              className={classes.fab}
              onClick={() => void handleDelete()}
              id="deleteCase"
              color="secondary"
            >
              <DeleteIcon />
            </Fab>
          </Tooltip>
        </div>
      )}

      <Divider />

      {thisCase.fileCategories.length > 0 && (
        <DataBox title="Materialarten">
          {thisCase.fileCategories.join(', ')}
        </DataBox>
      )}

      {thisCase.description && (
        <>
          <Container
            className={classes.container}
            maxWidth="md"
            component="section"
          >
            <Typography
              align="justify"
              style={{ whiteSpace: 'pre-line' }}
              variant="body1"
              id="caseDescription"
            >
              {thisCase.description}
            </Typography>
          </Container>
          <Divider />
        </>
      )}

      {thisCase.pedagogicFields.length > 0 && (
        <DataBox title="Pädagogisches Handlungsfeld / Arbeitsfeld">
          {thisCase.pedagogicFields.join(', ')}
        </DataBox>
      )}

      {thisCase.participants.length > 0 && (
        <DataBox title="Beteiligte">{thisCase.participants}</DataBox>
      )}

      {thisCase.problemAreas.length > 0 && (
        <DataBox title="Institution und Arbeitsfeld">
          {thisCase.problemAreas.join(', ')}
        </DataBox>
      )}

      <DataBox title="Fallmaterial">
        <CaseFiles thisCase={thisCase} />
      </DataBox>

      <Divider />

      <DataBox title="Schlagwörter">
        {thisCase.keywords.length === 0 ? (
          <Typography variant="body1" display="inline">
            keine
          </Typography>
        ) : (
          <div id="caseKeywords">
            {thisCase.keywords.map((keyword) => (
              <Chip
                key={keyword}
                className={classes.chip}
                label={keyword}
                onClick={() => history.push(`/suche/${keyword}`)}
                size="small"
                variant="outlined"
              />
            ))}
          </div>
        )}
      </DataBox>
    </div>
  );
};

export default CaseView;
