import React from 'react';
import { Box, LinearProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(1),
  },
  progress: {
    height: '0.4rem',
  },
}));

interface LinearProgressWithLabelProps {
  progress: number;
}

const LinearProgressWithLabel: React.FC<LinearProgressWithLabelProps> = ({
  progress,
}) => {
  const classes = useStyles();

  return (
    <Box alignItems="center" className={classes.root} display="flex">
      <Box width="100%" mr={1}>
        <LinearProgress
          className={classes.progress}
          color="primary"
          value={progress}
          variant="determinate"
        />
      </Box>
      <Box minWidth={35}>
        <Typography color="textSecondary" variant="body2">
          {progress}%
        </Typography>
      </Box>
    </Box>
  );
};

export default LinearProgressWithLabel;
