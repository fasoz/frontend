import React, { useContext, useState } from 'react';
import { Case, CaseFile, CaseFileCategories } from '../../../types';
import UserContext from '../../../UserContext';
import useSWR from 'swr';
import caseService from '../../../services/cases';
import { Button, Container } from '@material-ui/core';
import { Alert, AlertTitle, Skeleton } from '@material-ui/lab';
import SaveAltIcon from '@material-ui/icons/SaveAlt';
import FileCategoryPaper from './FileCategoryPaper';
import { downloadFile } from './lib';

interface CaseFilesProps {
  thisCase: Case;
  allowEdit?: boolean;
}

const CaseFiles: React.FC<CaseFilesProps> = ({ thisCase }) => {
  const { user } = useContext(UserContext);
  const { data: files, error } = useSWR<Array<CaseFile>>(
    user ? `/api/case/${thisCase.id}/file` : null,
    () => caseService.getFilesForCase(thisCase.id),
    { shouldRetryOnError: false },
  );
  const [zipDownloadButtonDisabled, setZipDownloadButtonDisabled] =
    useState<boolean>(false);

  if (user === null)
    return (
      <Container maxWidth="sm" style={{ textAlign: 'left' }}>
        <Alert severity="info">
          <AlertTitle>
            Falldateien können nur mit Zugangskonto abgerufen werden.
          </AlertTitle>
          Bitte melden Sie sich an.
        </Alert>
      </Container>
    );

  if (error?.request?.status && error.request.status === 403) {
    return (
      <Container maxWidth="sm">
        <Alert severity="info">
          <AlertTitle>Unzureichende Benutzer*innenrolle.</AlertTitle>
          Sie haben nicht die erforderlichen Berechtigungen um Fallmaterial für
          diesen Fall abrufen zu können.
        </Alert>
      </Container>
    );
  }

  if (!files || user === undefined)
    return (
      <div>
        <Skeleton
          variant="rect"
          height={150}
          width={600}
          style={{ margin: 'auto' }}
        />
      </div>
    );

  if (error) {
    return (
      <Container maxWidth="sm">
        <Alert severity="error">
          <AlertTitle>Fehler beim Laden der Dateien.</AlertTitle>
          {error.toString()}
        </Alert>
      </Container>
    );
  }

  if (files.length === 0) {
    return <div id="caseFiles">Keine Dateien vorhanden</div>;
  }

  const filesInFolder = (folder: string): Array<CaseFile> => {
    return files.filter((file) => file.path.startsWith(`${folder}/`));
  };

  const downloadZipFile = async () => {
    setZipDownloadButtonDisabled(true);
    await downloadFile(
      `/case/${thisCase.id}/files.zip`,
      `Fall ${thisCase.title}.zip`,
    );
    setZipDownloadButtonDisabled(false);
  };

  return (
    <Container maxWidth="sm" id="caseFiles">
      <Button
        color="secondary"
        disabled={zipDownloadButtonDisabled}
        onClick={() => void downloadZipFile()}
        startIcon={<SaveAltIcon />}
        variant="contained"
      >
        Als ZIP herunterladen
      </Button>
      {Object.values(CaseFileCategories).map((category) => {
        const filesInCategory = filesInFolder(category);
        if (filesInCategory.length === 0) return;
        return (
          <FileCategoryPaper
            key={category}
            thisCase={thisCase}
            files={filesInCategory}
          />
        );
      })}
    </Container>
  );
};

export default CaseFiles;
