import React from 'react';
import useSWR from 'swr';
import caseService from '../../../services/cases';
import {
  Container,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableRow,
  Tooltip,
  Typography,
} from '@material-ui/core';
import { Alert, AlertTitle, Skeleton } from '@material-ui/lab';
import filesize from 'filesize';
import { makeStyles } from '@material-ui/core/styles';
import DeleteIcon from '@material-ui/icons/Delete';
import SaveAltIcon from '@material-ui/icons/SaveAlt';
import { downloadFile } from './lib';
import * as types from '../../../types';
import CategoryDropzone from './CategoryDropzone';

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: theme.spacing(2),
    padding: theme.spacing(1),
  },
  actionCell: {
    whiteSpace: 'nowrap',
    width: '1%',
  },
}));

interface EditCaseFilesProps {
  caseId: string;
}

const EditCaseFiles: React.FC<EditCaseFilesProps> = ({ caseId }) => {
  const classes = useStyles();

  const {
    data: files,
    error,
    mutate,
  } = useSWR<Array<types.CaseFile>>(`/api/case/${caseId}/file`, () =>
    caseService.getFilesForCase(caseId),
  );

  if (!files)
    return (
      <div>
        <Skeleton
          variant="rect"
          height={150}
          width={600}
          style={{ margin: 'auto' }}
        />
      </div>
    );

  if (error) {
    return (
      <Container maxWidth="sm">
        <Alert severity="error">
          <AlertTitle>Fehler beim Laden der Dateien.</AlertTitle>
          {error.toString()}
        </Alert>
      </Container>
    );
  }

  const handleAddFile = async (
    category: string,
    uploadFiles: Array<File>,
    setUploadProgress: React.Dispatch<React.SetStateAction<number | null>>,
  ) => {
    const newFiles: Array<types.CaseFile> = [];
    for (let i = 0; i < uploadFiles.length; i++) {
      await caseService.addFileToCase(
        caseId,
        category,
        uploadFiles[i],
        (progressEvent) =>
          setUploadProgress(
            Math.round(
              (i * 100 + (progressEvent.loaded * 100) / progressEvent.total) /
                uploadFiles.length,
            ),
          ),
      );
      newFiles.push({
        path: uploadFiles[i].name,
        size: uploadFiles[i].size,
      });
    }
    await mutate([...files, ...newFiles]);
    setUploadProgress(null);
  };

  const handleDeleteFile = async (filename: string) => {
    if (
      !window.confirm(`Möchten Sie die Datei "${filename}" wirklich löschen?`)
    )
      return;
    await caseService.deleteFileFromCase(caseId, filename);
    await mutate(files.filter((c: types.CaseFile) => c.path !== filename));
  };

  return (
    <div id="caseFiles">
      {Object.values(types.CaseFileCategories).map((category) => {
        const filesInCategory = files.filter((file) =>
          file.path.startsWith(category),
        );
        return (
          <Paper className={classes.root} id={`${category}`} key={category}>
            <Typography variant="h5">{category}</Typography>

            <Table>
              <TableBody>
                {filesInCategory.map((file) => (
                  <TableRow key={file.path}>
                    <TableCell className={classes.actionCell}>
                      <Tooltip title="Löschen">
                        <IconButton
                          className="buttonDelete"
                          color="secondary"
                          onClick={() => void handleDeleteFile(file.path)}
                        >
                          <DeleteIcon />
                        </IconButton>
                      </Tooltip>
                      <Tooltip title="Herunterladen">
                        <IconButton
                          className="buttonDownload"
                          color="secondary"
                          onClick={() =>
                            void downloadFile(
                              `/case/${caseId}/file/${file.path}`,
                            )
                          }
                        >
                          <SaveAltIcon />
                        </IconButton>
                      </Tooltip>
                    </TableCell>
                    <TableCell>{file.path.split('/', 2)[1]}</TableCell>
                    <TableCell align="right">
                      {filesize(file.size, { standard: 'iec' })}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>

            <CategoryDropzone
              category={category}
              handleAddFile={handleAddFile}
            />
          </Paper>
        );
      })}
    </div>
  );
};

export default EditCaseFiles;
