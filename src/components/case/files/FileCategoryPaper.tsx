import React from 'react';
import { Case, CaseFile } from '../../../types';
import {
  IconButton,
  List,
  ListItem,
  ListItemText,
  Paper,
  Tooltip,
  Typography,
} from '@material-ui/core';
import filesize from 'filesize';
import SaveAltIcon from '@material-ui/icons/SaveAlt';
import { makeStyles } from '@material-ui/core/styles';
import { downloadFile, extractFileNameFromPath } from './lib';
import { ListItemSecondaryAction } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  paper: {
    height: '100%',
    padding: theme.spacing(1),
    marginTop: theme.spacing(1),
  },
}));

interface FileCategoryPaperProps {
  thisCase: Case;
  files: Array<CaseFile>;
}

const FileCategoryPaper: React.FC<FileCategoryPaperProps> = ({
  thisCase,
  files,
}) => {
  const classes = useStyles();

  const category = files[0].path.split('/')[0];

  return (
    <Paper className={classes.paper} variant="outlined">
      <Typography variant="h6">{category}</Typography>
      <List>
        {files.map((file) => (
          <ListItem
            button
            key={file.path}
            onClick={() =>
              void downloadFile(
                `/case/${thisCase.id}/file/${file.path}`,
                `Fall ${thisCase.title} - ${extractFileNameFromPath(
                  file.path,
                )}`,
              )
            }
          >
            <ListItemText
              primary={file.path.substr(category.length + 1)}
              secondary={filesize(file.size, {
                fullform: true,
                locale: 'de',
                standard: 'iec',
              })}
            />
            <ListItemSecondaryAction>
              <Tooltip title="Datei Herunterladen">
                <IconButton
                  color="secondary"
                  edge="end"
                  onClick={() =>
                    void downloadFile(
                      `/case/${thisCase.id}/file/${file.path}`,
                      `Fall ${thisCase.title} - ${extractFileNameFromPath(
                        file.path,
                      )}`,
                    )
                  }
                >
                  <SaveAltIcon />
                </IconButton>
              </Tooltip>
            </ListItemSecondaryAction>
          </ListItem>
        ))}
      </List>
    </Paper>
  );
};

export default FileCategoryPaper;
