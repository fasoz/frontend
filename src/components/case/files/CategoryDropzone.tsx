import React, { useState } from 'react';
import Dropzone, { FileError, FileRejection } from 'react-dropzone';
import { alpha, makeStyles, Theme } from '@material-ui/core/styles';
import LinearProgressWithLabel from './LinearProgressWithLabel';
import { Alert, AlertTitle } from '@material-ui/lab';
import { Container } from '@material-ui/core';
import filesize from 'filesize';

const useStyles = makeStyles((theme: Theme) => ({
  dropzone: {
    width: '100%',
    textAlign: 'center',
    padding: '10px',
    borderWidth: 3,
    borderRadius: 5,
    borderColor: theme.palette.secondary.dark,
    borderStyle: 'dashed',
    backgroundColor: alpha(theme.palette.secondary.main, 0.2),
    color: theme.palette.common.black,
    outline: 'none',
    transition: 'border .24s ease-in-out',
    cursor: 'pointer',
  },
  error: {
    boxShadow: '0 3px 5px 2px rgba(255, 105, 135, .3)',
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

interface CategoryDropzoneProps {
  category: string;
  handleAddFile: (
    category: string,
    uploadFiles: Array<File>,
    setUploadProgress: React.Dispatch<React.SetStateAction<number | null>>,
  ) => Promise<void>;
}

const maxFileUploadSize = 4 * Math.pow(1000, 3); // 4 GB
const maxFileUploadSizeHumanReadable = filesize(maxFileUploadSize, {
  locale: 'de',
  standard: 'iec',
});

const dropzoneFileErrorToTranslatedString = (err: FileError): string => {
  if (err.code === 'file-too-large')
    return `Datei ist größer als die maximal erlaubten ${maxFileUploadSizeHumanReadable}`;
  else if (err.code === 'file-too-small') return 'Dateigröße zu gering';
  else if (err.code === 'file-invalid-type') return 'Ungültiger Dateityp';
  else if (err.code === 'too-many-files') return 'Zu viele Dateien';
  else return err.message;
};

const CategoryDropzone: React.FC<CategoryDropzoneProps> = ({
  category,
  handleAddFile,
}) => {
  const classes = useStyles();

  const [uploadProgress, setUploadProgress] = useState<number | null>(0);
  const [error, setError] = useState<JSX.Element>();

  const handleReject = (fileRejections: Array<FileRejection>) => {
    setError(
      <>
        {fileRejections.map((rejection, index) => (
          <div key={index}>
            <i>{rejection.file.name}</i>:{' '}
            {rejection.errors
              .map((err) => dropzoneFileErrorToTranslatedString(err))
              .join(', ')}
          </div>
        ))}
      </>,
    );
  };

  return (
    <>
      <Dropzone
        disabled={!!uploadProgress}
        onDropAccepted={(uploadFiles) => {
          setError(undefined);
          void handleAddFile(category, uploadFiles, setUploadProgress);
        }}
        onDropRejected={handleReject}
        maxSize={maxFileUploadSize}
      >
        {({ getRootProps, getInputProps }) => (
          <div {...getRootProps()} className={classes.dropzone}>
            <input {...getInputProps()} />
            {uploadProgress ? (
              <LinearProgressWithLabel
                progress={uploadProgress ? uploadProgress : 0}
              />
            ) : (
              <p>
                Dateien in dieses Feld ziehen oder Feld anklicken um Dateien
                hochzuladen
              </p>
            )}
          </div>
        )}
      </Dropzone>
      {error && (
        <Container maxWidth="sm">
          <Alert severity="error" className={classes.error}>
            <AlertTitle>Hochladen nicht möglich</AlertTitle>
            {error}
          </Alert>
        </Container>
      )}
    </>
  );
};
export default CategoryDropzone;
