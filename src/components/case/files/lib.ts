import axios from 'axios';

export const downloadFile = async (
  path: string,
  overrideFileName: string | undefined = undefined,
): Promise<void> => {
  const response = await axios.request({
    method: 'GET',
    url: path,
    responseType: 'blob',
  });
  const downloadUrl = window.URL.createObjectURL(new Blob([response.data]));
  const link = document.createElement('a');
  link.href = downloadUrl;

  let fileName = overrideFileName;
  if (!fileName) {
    fileName = extractFileNameFromPath(path);
  }

  link.setAttribute('download', fileName);

  document.body.appendChild(link);
  link.click();
  link.remove();
};

export const extractFileNameFromPath = (path: string): string => {
  const pathSplit = path.split('/');
  return pathSplit[pathSplit.length - 1];
};
