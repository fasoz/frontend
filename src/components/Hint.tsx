import React, { useState } from 'react';
import { Button, IconButton, Modal, Paper } from '@material-ui/core';
import HelpIcon from '@material-ui/icons/Help';
import InfoIcon from '@material-ui/icons/Info';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    float: 'left',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[1],
    padding: theme.spacing(2),
  },
  textField: {
    marginBottom: theme.spacing(1),
  },
  formActions: {
    textAlign: 'right',
  },
  button: {
    marginLeft: theme.spacing(1),
  },
}));

interface HintProps {
  type?: 'info' | 'help';
}

const Hint: React.FC<HintProps> = ({ type = 'help', children }) => {
  const classes = useStyles();
  const [modalOpen, setModalOpen] = useState(false);

  return (
    <>
      <IconButton
        color="primary"
        size="small"
        onClick={() => setModalOpen(true)}
      >
        {type === 'help' ? <HelpIcon /> : <InfoIcon />}
      </IconButton>

      <Modal open={modalOpen} onClose={() => setModalOpen(false)}>
        <Paper className={classes.paper}>
          {children}
          <br />
          <Button
            fullWidth
            variant="contained"
            onClick={() => setModalOpen(false)}
          >
            schließen
          </Button>
        </Paper>
      </Modal>
    </>
  );
};

export default Hint;
