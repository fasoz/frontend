import React from 'react';
import { Typography } from '@material-ui/core';

const PageNotFound = (): JSX.Element => {
  return (
    <>
      <Typography align="center" variant="h4">
        Seite nicht gefunden
      </Typography>
      <Typography align="center" variant="h2">
        404
      </Typography>
    </>
  );
};

export default PageNotFound;
