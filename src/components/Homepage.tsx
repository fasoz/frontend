import React from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { Container, Divider, Grid, Link, Typography } from '@material-ui/core';
import { alpha, makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  buzz: {
    fontFamily: 'Exo',
    fontVariant: 'small-caps',
    fontWeight: 'bold',
  },
  divider: {
    backgroundColor: alpha(theme.palette.secondary.main, 0.5),
    margin: theme.spacing(2, 0, 2, 0),
  },
}));

const Homepage: React.FC = () => {
  const classes = useStyles();

  return (
    <Container maxWidth="md">
      <Typography align="center" gutterBottom variant="h4">
        Willkommen im Fallarchiv Soziale Arbeit
      </Typography>

      <Grid container alignItems="center" direction="row" component="section">
        <Grid item xs={12}>
          <Divider className={classes.divider} />
        </Grid>

        <Grid item xs={12} sm={4}>
          <Typography align="center" className={classes.buzz} variant="h5">
            Ziel
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <Typography align="justify" variant="body1" gutterBottom>
            Mit diesem Fallarchiv wenden wir uns an Studierende, Lehrende und
            Weiterbildende, als auch Forschende ebenso wie Praktiker*innen in
            den Handlungsfeldern der Sozialen Arbeit und auch generell
            Interessierte, denen ein einfacher Zugriff auf Fallmaterial
            ermöglicht werden soll.
          </Typography>
        </Grid>
      </Grid>

      <Divider className={classes.divider} />

      <Grid
        container
        alignItems="center"
        direction="row-reverse"
        component="section"
      >
        <Grid item xs={12} sm={4}>
          <Typography align="center" variant="h5" className={classes.buzz}>
            Angebot
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <Typography align="justify" variant="body1" gutterBottom>
            Ihnen möchten wir Fallmaterialien, sowohl in originaler Fassung als
            auch bereits interpretiertes, hochschuldidaktisch aufbereitetes
            Fallmaterial zur Verfügung stellen, um es für die (eigene)
            Weiterbildung, Forschung und Lehre nutzbar zu machen. Die
            gesammelten Materialien umfassen dabei vielfältige Typen von
            Fallportfolios aus den Bereichen der Einzelfall-, der sozialen
            Gruppen- sowie der Gemeinwesenarbeit.
          </Typography>
        </Grid>
      </Grid>

      <Divider className={classes.divider} />

      <Grid container alignItems="center" direction="row" component="section">
        <Grid item xs={12} sm={4}>
          <Typography align="center" className={classes.buzz} variant="h5">
            Lehre
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <Typography align="justify" variant="body1" gutterBottom>
            Die angelegten Materialien sollen zu einem reflexiven Umgang mit
            pädagogischen Situationen und Fällen anregen, den Reflexions- und
            Verstehenshorizont von Sozialarbeiter*innen erweitern und damit zu
            einem verbesserten Fallverstehen beitragen. Ziel des Fallarchives
            Soziale Arbeit ist die Anregung von individuellen sowie kollektiven
            Professionalisierungsprozessen, die zu einer Verquickung von Theorie
            und Praxis in entsprechender Auslegung anhand des Einzelfalls
            beitragen.
          </Typography>
        </Grid>
      </Grid>

      <Divider className={classes.divider} />

      <Grid
        container
        alignItems="center"
        direction="row-reverse"
        component="section"
      >
        <Grid item xs={12} sm={4}>
          <Typography align="center" className={classes.buzz} variant="h5">
            Kontakt
          </Typography>
        </Grid>
        <Grid item xs={12} sm={8}>
          <Typography align="justify" variant="body1" gutterBottom>
            Sie haben Fragen oder Anregungen? Nehmen Sie{' '}
            <Link component={RouterLink} to="/kontakt">
              Kontakt
            </Link>{' '}
            mit uns auf!
          </Typography>

          <Typography variant="body1" gutterBottom>
            Fallarchiv Soziale Arbeit, Frankfurt University of Applied Sciences,
            Nibelungenplatz 1, 60318 Frankfurt am Main
          </Typography>
        </Grid>
      </Grid>
    </Container>
  );
};

export default Homepage;
