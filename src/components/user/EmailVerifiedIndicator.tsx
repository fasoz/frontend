import React from 'react';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import { makeStyles, Tooltip } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
  yes: {
    color: theme.palette.success.main,
  },
  no: {
    color: theme.palette.warning.main,
  },
}));

const EmailVerifiedIndicator: React.FC<{ isVerified: boolean }> = ({
  isVerified,
}) => {
  const classes = useStyles();

  if (isVerified)
    return (
      <Tooltip
        title="Email wurde erfolgreich verifiziert"
        placement="bottom-start"
      >
        <CheckCircleOutlineIcon className={classes.yes} fontSize="small" />
      </Tooltip>
    );

  return (
    <Tooltip
      title="Email-Verifizierung wurde bisher nicht abgeschlossen."
      placement="bottom-start"
    >
      <ErrorOutlineIcon className={classes.no} fontSize="small" />
    </Tooltip>
  );
};

export default EmailVerifiedIndicator;
