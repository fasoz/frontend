import React from 'react';
import { Form, Formik, Field, FieldAttributes, useField } from 'formik';
import * as yup from 'yup';
import {
  Checkbox,
  Typography,
  TextField,
  Button,
  TextFieldProps,
  Paper,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CancelIcon from '@material-ui/icons/Cancel';
import SaveIcon from '@material-ui/icons/Save';
import { NewOrEditedUser, User, UserRole } from '../../types';
import Hint from '../Hint';
import RoleDescriptions from './RoleDescriptions';
import { roleTranslations } from '../../utils/roleTranslation';

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    float: 'left',
    left: '50%',
    top: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
  textField: {
    marginBottom: theme.spacing(1),
  },
  formActions: {
    textAlign: 'right',
  },
  button: {
    marginLeft: theme.spacing(1),
  },
}));

const CustomTextField: React.FC<
  TextFieldProps & FieldAttributes<Record<string, unknown>>
> = ({ label, ...props }) => {
  const classes = useStyles();
  const [field, meta] = useField<Record<string, unknown>>(props);
  const errorText = meta.error && meta.touched ? meta.error : '';
  return (
    <TextField
      {...field}
      type={props.type}
      label={label}
      className={classes.textField}
      fullWidth
      error={Boolean(errorText)}
      helperText={errorText}
    />
  );
};

interface UserFormProps {
  editUser?: User;
  closeModal(): void;
  handleAddUser(newUser: NewOrEditedUser): Promise<void>;
  handleEditUser(userId: string, editedUser: NewOrEditedUser): Promise<void>;
}

/**
 * Reusable Form for adding and editing users
 * @param closeModal - callback that closes the modal this form is wrapped in
 * @param handleAddUser - function to call when adding a user
 * @param handleEditUser - function to call when editing a user
 * @param editUser - optional user object that's only need when editing a user.
 * @constructor
 */
const UserForm: React.FC<UserFormProps> = ({
  closeModal,
  handleAddUser,
  handleEditUser,
  editUser = undefined,
}) => {
  const classes = useStyles();

  const validationSchema = editUser
    ? yup.object({
        name: yup.string(),
        username: yup.string().required().email().min(3),
        password: yup.string().min(12),
        passwordConfirmation: yup
          .string()
          .oneOf(
            [yup.ref('password'), undefined],
            'Passwörter stimmen nicht überein',
          ),
        roles: yup.array().of(yup.mixed().oneOf(Object.values(UserRole))),
      })
    : yup.object({
        name: yup.string(),
        username: yup.string().required().email().min(3),
        password: yup.string().required().min(12),
        passwordConfirmation: yup
          .string()
          .required()
          .oneOf([yup.ref('password'), ''], 'Passwörter stimmen nicht überein'),
        roles: yup.array().of(yup.mixed().oneOf(Object.values(UserRole))),
      });

  const initialValues: NewOrEditedUser = editUser
    ? {
        name: editUser.name,
        username: editUser.username,
        password: '',
        roles: editUser.roles,
      }
    : {
        name: '',
        username: '',
        password: '',
        roles: [],
      };

  return (
    <Paper className={classes.paper}>
      <Typography variant="h4" align="center">
        {editUser ? 'Konto bearbeiten' : 'Konto hinzufügen'}
      </Typography>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values, { setSubmitting, resetForm }) => {
          setSubmitting(true);

          const userToSubmit: NewOrEditedUser = {
            name: values.name,
            username: values.username,
            password: values.password,
            roles: values.roles,
          };
          if (editUser) {
            void handleEditUser(editUser.id, userToSubmit);
            closeModal();
          } else {
            void handleAddUser(userToSubmit);
            resetForm();
          }

          setSubmitting(false);
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <Field
              name="name"
              type="input"
              as={CustomTextField}
              label="Vor- und Nachname"
            />
            <Field
              name="username"
              type="input"
              as={CustomTextField}
              label="Email-Adresse"
            />
            <Field
              name="password"
              type="password"
              as={CustomTextField}
              label="Passwort"
            />
            <Field
              name="passwordConfirmation"
              type="password"
              as={CustomTextField}
              label="Passwort bestätigen"
            />
            Rollen:
            <Hint>
              <RoleDescriptions />
            </Hint>
            {roleTranslations.map((role) => (
              <label key={role.internalName}>
                <Field
                  name="roles"
                  type="checkbox"
                  as={Checkbox}
                  value={role.internalName}
                />
                {role.translatedName}
              </label>
            ))}
            <div className={classes.formActions}>
              <Button
                className={classes.button}
                onClick={closeModal}
                startIcon={<CancelIcon />}
                variant="contained"
              >
                Abbrechen
              </Button>
              <Button
                className={classes.button}
                color="primary"
                disabled={isSubmitting}
                startIcon={<SaveIcon />}
                type="submit"
                variant="contained"
              >
                {editUser ? 'Speichern' : 'Hinzufügen'}
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </Paper>
  );
};

export default UserForm;
