import React from 'react';
import { Typography } from '@material-ui/core';

import { roleToTranslatedString } from '../../utils/roleTranslation';
import { UserRole } from '../../types';

const RoleDescriptions: React.FC = () => {
  return (
    <>
      <Typography align="center" variant="h5">
        Beschreibung der Rollen
      </Typography>

      <Typography variant="h6">
        {roleToTranslatedString(UserRole.Guest)}
      </Typography>
      <Typography variant="body2" gutterBottom>
        <ul>
          <li>Darf Falldateien herunterladen.</li>
          <li>Sonst keine Unterschiede zu nicht-angemeldeten NutzerInnen.</li>
        </ul>
      </Typography>

      <Typography variant="h6">
        {roleToTranslatedString(UserRole.Researcher)}
      </Typography>
      <Typography variant="body2" gutterBottom>
        <ul>
          <li>Darf neue Fälle anlegen.</li>
          <li>Darf selbst angelegte Fälle bearbeiten und löschen.</li>
          <li>
            Darf <u>keine</u> Fälle veröffentlichen. Neu angelegte Fälle sind
            immer nicht-öffentliche Entwürfe.
          </li>
          <li>
            Darf Fälle anderer <u>nicht</u> bearbeiten oder löschen.
          </li>
        </ul>
      </Typography>

      <Typography variant="h6">
        {roleToTranslatedString(UserRole.Editor)}
      </Typography>
      <Typography variant="body2" gutterBottom>
        <ul>
          <li>
            Darf Fälle veröffentlichen. Sowohl selbst angelegte Fälle, als auch
            Fälle anderer.
          </li>
          <li>Darf Fälle anderer bearbeiten und löschen.</li>
          <li>Kann unveröffentlichte Entwürfe sehen.</li>
        </ul>
      </Typography>

      <Typography variant="h6">
        {roleToTranslatedString(UserRole.Administrator)}
      </Typography>
      <Typography variant="body2" gutterBottom>
        <ul>
          <li>Darf Zugänge erstellen, bearbeiten und löschen.</li>
          <li>Darf Rollen zuweisen und entziehen.</li>
        </ul>
      </Typography>
    </>
  );
};

export default RoleDescriptions;
