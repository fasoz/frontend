import React, { useState } from 'react';
import {
  IconButton,
  Modal,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
} from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { NewOrEditedUser, User } from '../../types';
import EmailVerifiedIndicator from './EmailVerifiedIndicator';
import UserForm from './Form';
import { roleToTranslatedString } from '../../utils/roleTranslation';

interface UserListProps {
  users: Array<User>;
  handleDeleteUser(userId: User): Promise<void>;
  handleEditUser(userId: string, editUser: NewOrEditedUser): Promise<void>;
}

const UserList: React.FC<UserListProps> = ({
  users,
  handleDeleteUser,
  handleEditUser,
}) => {
  const [userToEdit, setUserToEdit] = useState<User>();

  return (
    <>
      <Modal open={!!userToEdit} onClose={() => setUserToEdit(undefined)}>
        <UserForm
          closeModal={() => setUserToEdit(undefined)}
          editUser={userToEdit}
          handleAddUser={() => Promise.resolve()}
          handleEditUser={handleEditUser}
        />
      </Modal>
      <Table id="usersTable">
        <TableHead>
          <TableRow>
            <TableCell />
            <TableCell>Name</TableCell>
            <TableCell>Email</TableCell>
            <TableCell>Rollen</TableCell>
            <TableCell>Fälle</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {users &&
            users.map((user) => (
              <TableRow key={user.id}>
                <TableCell>
                  <IconButton
                    className="editUserButton"
                    size="small"
                    onClick={() => setUserToEdit(user)}
                  >
                    <EditIcon />
                  </IconButton>
                  <IconButton
                    className="deleteUserButton"
                    size="small"
                    onClick={() => void handleDeleteUser(user)}
                  >
                    <DeleteIcon />
                  </IconButton>
                </TableCell>
                <TableCell>{user.name}</TableCell>
                <TableCell>
                  <EmailVerifiedIndicator isVerified={user.emailVerified} />{' '}
                  {user.username}
                </TableCell>
                <TableCell>
                  {user.roles
                    .map((role) => roleToTranslatedString(role))
                    .join(', ')}
                </TableCell>
                <TableCell>{user.cases.length}</TableCell>
              </TableRow>
            ))}
        </TableBody>
      </Table>
    </>
  );
};

export default UserList;
