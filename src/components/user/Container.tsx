import React, { useContext, useState } from 'react';
import useSWR from 'swr';
import { Fab, Typography, Modal } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import AddIcon from '@material-ui/icons/Add';
import { AlertMessage, NewOrEditedUser, User } from '../../types';
import UserContext from '../../UserContext';
import usersService from '../../services/users';
import UserForm from './Form';
import UserList from './List';
import { makeStyles } from '@material-ui/core/styles';
import LoadingIndicator from '../layout/LoadingIndicator';

const useStyles = makeStyles(() => ({
  root: {
    position: 'relative',
  },
  actionButton: {
    position: 'absolute',
    top: 0,
    right: 0,
  },
}));

const UserContainer: React.FC = () => {
  const classes = useStyles();

  const { user } = useContext(UserContext);
  const {
    data: users,
    mutate,
    error,
  } = useSWR(user ? '/api/user' : null, usersService.getAll, {
    onErrorRetry: (error) => {
      if (error.status === 403) return;
    },
  });
  const [alertMessage, setAlertMessage] = useState<AlertMessage>();
  const [userFormModalOpen, setUserFormModalOpen] = useState(false);

  if (user === undefined)
    return <LoadingIndicator text="Authentifizierung wird überprüft" />;

  if (user === null)
    return (
      <Alert severity="error">
        <AlertTitle>Zugriff verweigert</AlertTitle>
        Sie sind nicht angemeldet.
      </Alert>
    );

  if (user && !user.roles.includes('Administrator'))
    return (
      <Alert severity="error">
        <AlertTitle>Zugriff verweigert</AlertTitle>
        Ihnen fehlen die erforderlichen Berechtigungen.
      </Alert>
    );

  if (!users)
    return <LoadingIndicator text="Liste der Nutzerkonten wird geladen" />;

  const handleAddUser = async (newUser: NewOrEditedUser): Promise<void> => {
    try {
      const response = await usersService.createUser(newUser);
      await mutate(
        [response, ...users].sort((a: User, b: User) => {
          if (a.name < b.name) return -1;
          if (a.name > b.name) return 1;
          if (a.username < b.username) return -1;
          if (a.username > b.username) return 1;
          return 0;
        }),
        false,
      );
    } catch (error) {
      setAlertMessage({
        severity: 'error',
        title: 'Fehler beim Hinzufügen des Kontos.',
        message: error.response.data.error || error.toString(),
      });
    }
    setUserFormModalOpen(false);
  };

  const handleEditUser = async (
    userId: string,
    editedUser: NewOrEditedUser,
  ): Promise<void> => {
    if (!editedUser.password) delete editedUser.password; // leave password unchanged if field is left empty
    try {
      const response = await usersService.editUser(userId, editedUser);
      await mutate(
        users.map((u: User) => (u.id === userId ? response : u)),
        false,
      );
    } catch (error) {
      setAlertMessage({
        severity: 'error',
        title: 'Fehler beim Bearbeiten des Kontos.',
        message: error.response.data.error || error.toString(),
      });
    }
  };

  const handleDeleteUser = async (user: User) => {
    if (!window.confirm(`Konto "${user.username}" löschen?`)) return;
    try {
      await usersService.deleteUser(user.id);
      await mutate(
        users.filter((u: User) => u.id !== user.id),
        false,
      );
    } catch (error) {
      setAlertMessage({
        severity: 'error',
        title: 'Fehler beim Löschen des Kontos.',
        message: error.response.data.error || error.toString(),
      });
    }
  };

  if (error) {
    return (
      <Alert severity="error">
        <AlertTitle>Fehler beim Laden der Zugangskonten.</AlertTitle>
        {error.toString()}
      </Alert>
    );
  }

  return (
    <div className={classes.root}>
      <Modal
        open={userFormModalOpen}
        onClose={() => setUserFormModalOpen(false)}
      >
        <UserForm
          closeModal={() => setUserFormModalOpen(false)}
          handleAddUser={handleAddUser}
          handleEditUser={handleEditUser}
        />
      </Modal>
      <Typography gutterBottom variant="h4">
        Zugangskonten
      </Typography>
      <Fab
        className={classes.actionButton}
        color="secondary"
        id="addUserButton"
        variant="extended"
        onClick={() => setUserFormModalOpen(true)}
      >
        <AddIcon /> Konto hinzufügen
      </Fab>
      {alertMessage && (
        <Alert severity={alertMessage.severity}>
          {alertMessage.title && <AlertTitle>{alertMessage.title}</AlertTitle>}
          {alertMessage.message}
        </Alert>
      )}
      <UserList
        users={users}
        handleDeleteUser={handleDeleteUser}
        handleEditUser={handleEditUser}
      />
    </div>
  );
};

export default UserContainer;
