import React, { useContext } from 'react';
import useSWR from 'swr';
import adminService from '../../services/admin';
import LoadingIndicator from '../layout/LoadingIndicator';
import UserContext from '../../UserContext';
import { Alert, AlertTitle } from '@material-ui/lab';
import { Button, Container } from '@material-ui/core';
import { Typography } from '@material-ui/core/';

const AdminElasticsearch: React.FC = () => {
  const { user } = useContext(UserContext);

  const { data, error, mutate } = useSWR<Record<string, number>>(
    user ? '/admin/elasticsearch/stats' : null,
    adminService.elasticsearchStats,
  );

  if (user === undefined)
    return <LoadingIndicator text="Authentifizierung wird überprüft" />;

  if (user === null)
    return (
      <Alert severity="error">
        <AlertTitle>Zugriff verweigert</AlertTitle>
        Sie sind nicht angemeldet.
      </Alert>
    );

  if (error) return <>{error}</>;

  if (!data) return <LoadingIndicator text="Lade Elasticsearch-Statistiken" />;

  const handleSynchronize = async () => {
    await adminService.elasticsearchRebuildIndex();
    await mutate();
  };

  return (
    <Container maxWidth="sm">
      <Typography gutterBottom variant="h4">
        Elasticsearch
      </Typography>
      <Typography align="center" paragraph variant="body1">
        Dokumente im Suchindex
        <br />
        Anzahl: {data.count}
        <br />
        Gelöscht: {data.deleted}
      </Typography>
      <Typography paragraph variant="body1">
        Elasticsearch ist die Software die für die Fallsuche eingesetzt wird.
        Dazu wird automatisch ein Suchindex gepflegt der immer identisch mit den
        Fällen in der Falldatenbank (MongoDB) sein sollte. Wenn der Suchindex
        doch einmal fehlerhaft ist, klicken Sie den Knopf unten.
      </Typography>
      <div style={{ textAlign: 'center' }}>
        <Button
          color="secondary"
          onClick={() => void handleSynchronize()}
          variant="contained"
        >
          Suchindex synchronisieren
        </Button>
      </div>
    </Container>
  );
};

export default AdminElasticsearch;
