import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import { Menu, MenuItem } from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';

const AdminMenu: React.FC = () => {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const links = [
    { href: '/zugangsverwaltung', text: 'Zugangsverwaltung' },
    { href: '/admin/elasticsearch', text: 'Elasticsearch' },
  ];

  return (
    <>
      <Button
        color="secondary"
        onClick={handleClick}
        size="small"
        variant="contained"
      >
        Admin
      </Button>
      <Menu
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        getContentAnchorEl={null}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        {links.map((link) => (
          <MenuItem
            component={RouterLink}
            key={link.href}
            to={link.href}
            onClick={handleClose}
          >
            {link.text}
          </MenuItem>
        ))}
      </Menu>
    </>
  );
};

export default AdminMenu;
