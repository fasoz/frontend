import React from 'react';
import {
  Container,
  createStyles,
  Divider,
  Link,
  Typography,
} from '@material-ui/core';
import { makeStyles, Theme } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    citationNumber: {
      color: theme.palette.primary.dark,
    },
  }),
);

const EthicsCodex: React.FC = () => {
  const classes = useStyles();

  return (
    <Container maxWidth="md">
      <Typography align="center" gutterBottom variant="h4">
        Ethik-Kodex
      </Typography>

      <Typography align="center" gutterBottom variant="h5">
        Zum professionsethischen Umgang mit dem Fallmaterial
      </Typography>

      <Typography align="justify" variant="body1" paragraph>
        Das Fallarchiv Soziale Arbeit (FaSoz) versteht sich als Archiv zur
        Sammlung, analytischen Aufbereitung und Zugänglichmachung von Fällen aus
        den Handlungsfeldern der Sozialen Arbeit, die in vollständig
        anonymisierter Form zur Verfügung gestellt werden. Ziel ist es sowohl
        individuelle als auch kollektive Lern- und Professionalisierungsprozesse
        innerhalb der Handlungsfelder zu befördern und damit einen Beitrag zu
        einem funktionierenden Wissenschaftstransfer in einem Arbeitsfeld zu
        leisten, in dem Fall- und Selbstreflexion einen grundlegenden
        Bestandteil sowohl wissenschaftlicher als auch sozialarbeiterischer
        Praxis darstellen. Die ethische Vertretbarkeit der Materialsicherung und
        der Schutz{' '}
        <Link component={RouterLink} to="/datenschutzerklärung">
          personenbezogener Daten
        </Link>{' '}
        als auch vertraulicher Informationen entsprechend datenschutzrechtlicher
        Bestimmungen sind dabei zentral. Angaben, die Rückschlüsse auf die am
        Fall Beteiligten ermöglichen wurden vollständig anonymisiert bzw.
        pseudonymisiert. Wenn Sie meinen einen Bezug zu einem ihnen bekannten
        Fall herstellen zu können, nehmen Sie bitte Kontakt mit uns auf.
      </Typography>

      <Typography align="justify" variant="body1" paragraph>
        Der vorliegende Ethik-Kodex zielt im Sinne einer Selbstverpflichtung auf
        einen verantwortungsvollen und einwandfreien Umgang der Nutzer:innen mit
        den angelegten Fallmaterialien sowohl aus professionsethischer als auch
        datenschutzrechtlicher Sicht. Die Prinzipien des Umgangs mit dem
        Fallarchiv und den sich im Archiv befindlichen Fallmaterialien
        orientieren sich an dem Ethikkodex der Deutschen Gesellschaft für
        Soziale Arbeit<sup className={classes.citationNumber}>1</sup>, bzw. den
        Prinzipien der Sozialen Arbeit und ihrem Selbstverständnis als
        Menschenrechtsprofession2, deren Grundlage die Achtung vor dem
        besonderen Wert und der Würde aller Menschen und den sich daraus
        ergebenden Grundrechten darstellt. Das Fallarchiv versucht mit dem
        Fallmaterial die Lebenswirklichkeiten von Adressatinnen Sozialer Arbeit
        abzubilden und für die Reflexion zugänglich zu machen, immer wieder auch
        und insbesondere von marginalisierten und stigmatisierten
        Personen(gruppen) und Menschen in besonders vulnerablen und von
        Abhängigkeit geprägten Situationen. Auch bedenkliche oder problematische
        Inhalte sollen dabei in angemessenem Rahmen Gegenstand professioneller
        Reflexion werden können und der Weiterentwicklung der
        professionsethischen Haltung dienen. Dies bedeutet für uns und unsere
        Nutzer:innen die Fallbeteiligten und Falleinbringenden nicht auf
        Vulnerabilitäten zu reduzieren, sondern sie als Subjekte mit eigenen,
        subjektiv sinnvollen Entscheidungen innerhalb eines jeden ‚Falles‘
        anzuerkennen und gleichermaßen stets zu reflektieren, ob oder inwieweit
        wir/sie selbst zu stigmatisierenden Adressierungen beitragen und
        bestehende Machtungleichheitsverhältnisse reproduzieren. Die Materialien
        des Archivs enthalten sowohl Erkenntnisse über Lebenssituationen und
        mögliche Perspektiven von Adressatinnen und Fachkräften, als auch über
        Konzepte und Institutionen der sozialen Arbeit. Die Fälle sind so
        angelegt, dass sie verschiedene methodische Zugänge ermöglichen und
        damit auch eine Bandbreite unterschiedlicher Interpretationen zulassen.
        Ziel des Archivs ist ein dialogischer, kollaborationsbasierter
        Wissensaustausch, in dem sich eine Kultur der Reflexion etablieren kann
        und ethische Haltungen durchgängig geprüft und weiterentwickelt werden.
      </Typography>

      <Divider />

      <Typography variant="body2" gutterBottom>
        <sup className={classes.citationNumber}>1</sup> „Ziele von
        Menschenrechtsarbeit im Rahmen der Sozialen Arbeit sind auf der
        individuellen Ebene die Wiederherstellung von Menschenwürde sowie
        Wohlbefinden durch Bedürfnisbefriedigung und Lernprozesse, auf der
        gesellschaftlichen Ebene gesellschaftliche Integration, soziale
        Gerechtigkeit sowie sozialer Wandel in Anbetracht menschenverachtender
        sozialer Strukturen und Kulturmuster und – langfristig – die Arbeit an
        einer Menschenrechtskultur im Alltag“ (Staub-Bernasconi, Silvia: Soziale
        Arbeit: Dienstleistung oder Menschenrechtsprofession? Zum
        Selbstverständnis Sozialer Arbeit in Deutschland mit einem Seitenblick
        auf die internationale Diskussionslandschaft. In: Lob-Hüdepohl,
        Andreas/Lesch, Walter (Hg.) (2007): Ethik Sozialer Arbeit – Ein
        Handbuch: Einführung in die Ethik der Sozialen Arbeit, UTB/Schöningh: S.
        :20-54.
      </Typography>
    </Container>
  );
};

export default EthicsCodex;
