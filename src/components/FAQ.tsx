import React from 'react';
import {
  Container,
  Link,
  Typography,
  Table,
  TableBody,
  TableRow,
  TableCell,
} from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { Link as RouterLink } from 'react-router-dom';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    h6: {
      marginTop: theme.spacing(1),
    },
  }),
);

const FAQ: React.FC = () => {
  const classes = useStyles();

  return (
    <Container maxWidth="md">
      <Typography align="center" gutterBottom variant="h4">
        Nutzung der Website / Nutzung der Fälle
      </Typography>

      <article>
        <Typography variant="h6" className={classes.h6}>
          Wie bekomme ich Zugang zum Fallmaterial? Welche Zugangsbeschränkungen
          gibt es?
        </Typography>

        <Typography variant="body1">
          Der Großteil unseres Fallmaterials ist uneingeschränkt und frei
          zugänglich. Da jedoch auch Fallmaterial vorliegt, das gewissen
          Zugangsbeschränkungen unterliegt, möchten wir Sie bitten, sich
          zunächst als Nutzer*in im Fallarchiv{' '}
          <Link component={RouterLink} to="/registrieren">
            zu registrieren
          </Link>
          .
        </Typography>
      </article>

      <article>
        <Typography variant="h6" className={classes.h6}>
          Wie kann ich einen Fall herunterladen? Wie kann ich ihn nutzen?
        </Typography>

        <Typography variant="body1">
          Zur Auswahl der Fälle steht Ihnen zum einen eine{' '}
          <Link component={RouterLink} to="/fallverzeichnis">
            Fallauflistung in alphabetischer Reihenfolge der Titel
          </Link>{' '}
          zur Verfügung. Zudem können Sie die Suchfunktion nutzen und nach
          Schlüsselbegriffen suchen (siehe Suchfeld links oben). Die Dateien
          sind vorsortiert und umfassen zumeist folgende Materialarten:
        </Typography>

        <Table>
          <TableBody>
            <TableRow>
              <TableCell>Primärmaterial</TableCell>
              <TableCell>
                hierunter fällt das Originalmaterial zum Fall, wie z.B.
                Audiodateien, Transkripte, Datensätze und Originalakten
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Aufbereitetes Material</TableCell>
              <TableCell>
                meint z.B. Soziogramme, Genogramme, Zeitstrahl,
                Zusammenfassungen etc.
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Hintergrundmaterial</TableCell>
              <TableCell>
                hierunter fällt alles, das hilft den Fall zu verstehen, so z.B.
                Stadtteilbeschreibungen, Erläuterungen zu Krankheiten oder
                psychischen Störungsbildern, Fachliteratur zu vorkommenden
                Themenkomplexen
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Didaktisches Material</TableCell>
              <TableCell>
                meint Material zum Umgang mit dem Fall, Lehrmaterialien etc.
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>Fachliteratur</TableCell>
              <TableCell>
                meint Veröffentlichungen, die aus dem Fall hervorgegangen sind
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>

        <Typography variant="body1">
          Das Fallmaterial ist insbesondere für die Nutzung innerhalb der
          Hochschullehre, zur eigenen Fortbildung oder auch für weitere
          Fortbildungszwecke (unter Angabe des Quellenverweises) gedacht.
          Weiterveröffentlichungen des Materials in jeglicher Art sind
          untersagt.
        </Typography>
      </article>

      <article>
        <Typography variant="h6" className={classes.h6}>
          Ich möchte gerne eigenes Fallmaterial in das Fallarchiv einbringen. Wo
          finde ich Informationen?
        </Typography>

        <Typography variant="body1">
          Wir freuen uns, dass Sie Teil des Fallarchivs werden und Ihr eigenes
          Material einbringen möchten. Hierbei unterstützen wir Sie gerne. Sie
          finden alle Veröffentlichungsinformationen in der Handreichung zur
          Fallveröffentlichung.
          <br />
          Sollten Sie dennoch Fragen haben,{' '}
          <Link component={RouterLink} to="/kontakt">
            wenden Sie sich gerne an uns
          </Link>
          .
        </Typography>
      </article>
    </Container>
  );
};

export default FAQ;
