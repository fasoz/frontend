import React from 'react';
import { Container, Link, Typography } from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';

const PrivacyStatement: React.FC = () => {
  return (
    <Container maxWidth="md">
      <Typography variant="h4" gutterBottom>
        Datenschutzerklärung
      </Typography>

      <Typography variant="body1" gutterBottom>
        Wir nehmen Datenschutz sehr ernst, behandeln personenbezogene Daten
        vertraulich und entsprechend gesetzlicher Vorschriften.
      </Typography>

      <Typography variant="body1" gutterBottom>
        Diese Datenschutzerklärung informiert darüber unter welchen
        Voraussetzungen personenbezogene Daten gespeichert werden, wie sie
        verarbeitet werden und zu welchem Zweck sie erhoben und verarbeitet
        werden.
        <br />
        Die Begriffe &quot;personenbezogenen Daten&quot; und
        &quot;Verarbeitung&quot; werden entsprechend der Begriffsbestimmungen in
        Art. 4 DSGVO verwendet.
      </Typography>

      <Typography variant="h5" gutterBottom>
        Zugriffsdaten
      </Typography>

      <Typography variant="body1" gutterBottom>
        Wir, die Websitebetreiber*innen bzw. Seitenprovider*innen, erheben
        aufgrund unseres berechtigten Interesses (siehe Art. 6 Abs. 1 lit. f.
        DSGVO) Daten über Zugriffe auf die Website und speichern diese als
        “Server-Logfiles” auf dem Server der Website ab. Folgende Daten werden
        so protokolliert:
        <ul>
          <li>Besuchte Website</li>
          <li>Uhrzeit zum Zeitpunkt des Zugriffes</li>
          <li>Menge der gesendeten Daten in Byte</li>
          <li>Quelle/Verweis, von welchem Sie auf die Seite gelangten</li>
          <li>Verwendeter Browser</li>
          <li>Verwendetes Betriebssystem</li>
          <li>Verwendete IP-Adresse</li>
        </ul>
        Die Server-Logfiles werden für maximal 1 Monat gespeichert und
        anschließend gelöscht. Die Speicherung der Daten erfolgt aus
        Sicherheitsgründen, um z. B. Missbrauchsfälle aufklären zu können.
        Müssen Daten aus Beweisgründen aufgehoben werden, sind sie solange von
        der Löschung ausgenommen bis der Vorfall endgültig geklärt ist.
      </Typography>

      <Typography variant="h5" gutterBottom>
        Cookies
      </Typography>

      <Typography variant="body1" gutterBottom>
        Diese Webseite verwendet Cookies zur Authentifizierung angemeldeter
        Nutzer*innen. Dazu muss der/die Nutzer*in ein Zugangskonto haben und
        über das{' '}
        <Link component={RouterLink} to="/anmelden">
          Anmeldeformular
        </Link>{' '}
        angemeldet sein.
        <br />
        Wenn eine Nutzer*in nicht angemeldet ist, wird kein Cookie gesetzt. Die
        Nutzung der Webseite ist grundsätzlich ohne Anmeldung, und somit ohne
        gesetzte Cookies, möglich.
      </Typography>

      <Typography variant="h5" gutterBottom>
        Erfassung und Verarbeitung personenbezogener Daten
      </Typography>

      <Typography variant="body1" gutterBottom>
        Diese Website können Sie auch besuchen, ohne Angaben zu Ihrer Person zu
        machen. Bestimmte Funktionen sind jedoch nur für authentifizierte
        Nutzer*innen zugänglich. Ein Zugangskonto ist insbesondere zum Erstellen
        neuer Fälle und zum Herunterladen von Fallmaterial erforderlich.
      </Typography>

      <Typography variant="h6" gutterBottom>
        Erstellen eines Zugangskontos (Registrierung)
      </Typography>

      <Typography variant="body1" gutterBottom>
        Zur Erstellung eines Zugangskontos werden folgende personenbezogenen
        Daten erhoben:
        <ul>
          <li>
            Anzeigename z.B. der bürgerliche Name (kann leer gelassen werden)
          </li>
          <li>Email-Adresse</li>
          <li>Passwort</li>
        </ul>
        Anzeigename und Email-Adresse werden im Klartext gespeichert. Das
        Passwort wird nicht im Klartext gespeichert, sondern als so genannter
        Passwort-Hash. Das ist eine Art Prüfsumme, die zum Überprüfen des
        Passworts beim Anmelden genutzt werden kann, aber nicht in das
        eigentliche Passwort rückführbar ist.
        <br />
        Ein Zugangskonto ist Voraussetzung für eine Anmeldung/Authentifizierung
        auf der Webseite.
      </Typography>

      <Typography variant="h6" gutterBottom>
        Anmelden auf Webseite (Login)
      </Typography>

      <Typography variant="body1" gutterBottom>
        Zur Authentifizierung gegenüber der Webseite mittels eines Zugangskontos
        müssen Email-Adresse und Passwort eingegeben werden. Nach erfolgreicher
        Anmeldung beginnt eine so genannte Anmeldesitzung. Dazu wird ein Cookie
        gesetzt (siehe dazu Abschnitt &quot;Cookies&quot;). Außerdem werden
        Server-seitig folgende Daten zur jeweiligen Anmeldesitzung gespeichert:
        <ul>
          <li>Datum und Uhrzeit des Beginns der Sitzung</li>
          <li>
            Datum und Uhrzeit des letzten Zugriffs auf die Webseite innerhalb
            dieser Sitzung
          </li>
          <li>Verwendeter Browser</li>
          <li>Verwendetes Betriebssystem</li>
        </ul>
        Mit der Abmeldung auf der Webseite erfolgt die Löschung der Daten der
        aktuellen Sitzung. Es besteht außerdem die Möglichkeit unter{' '}
        <Link component={RouterLink} to="/meinkonto">
          Mein Konto
        </Link>{' '}
        durch drücken der Schaltfläche &quot;Auf allen Geräten abmelden&quot;
        alle Sitzungsdaten vollständig zu löschen.
      </Typography>

      <Typography variant="h6" gutterBottom>
        Erstellen und Bearbeiten von Fällen
      </Typography>

      <Typography variant="body1" gutterBottom>
        Wenn Sie als angemeldete Nutzer*in einen Fall erstellen oder bearbeiten
        wird gespeichert, dass Sie diesen Fall erstellt bzw. daran mitgearbeitet
        haben. Dies erfolgt aufgrund unserer berechtigten Interessen im Sinne
        des Art. 6 Abs. 1 lit. f. DSGVO und dient der Sicherheit von uns als
        Websitebetreiber*in: Denn sollte Ihre Kontribution gegen geltendes Recht
        verstoßen, können wir dafür belangt werden, weshalb wir ein Interesse an
        der Identität der Autor*in haben.
      </Typography>

      <Typography variant="h5" gutterBottom>
        Rechte der Nutzer*innen
      </Typography>

      <Typography variant="body1" gutterBottom>
        Sie haben als Nutzer das Recht, auf Antrag eine kostenlose Auskunft
        darüber zu erhalten, welche personenbezogenen Daten über Sie gespeichert
        wurden. Sie haben außerdem das Recht auf Berichtigung falscher Daten und
        auf die Verarbeitungseinschränkung oder Löschung Ihrer personenbezogenen
        Daten. Falls zutreffend, können Sie auch Ihr Recht auf Datenportabilität
        geltend machen. Sollten Sie annehmen, dass Ihre Daten unrechtmäßig
        verarbeitet wurden, können Sie eine Beschwerde bei der zuständigen
        Aufsichtsbehörde einreichen.
      </Typography>

      <Typography variant="h6" gutterBottom>
        Löschung von Daten
      </Typography>

      <Typography variant="body1" gutterBottom>
        Sofern Ihr Wunsch nicht mit einer gesetzlichen Pflicht zur Aufbewahrung
        von Daten (z. B. Vorratsdatenspeicherung) kollidiert, haben Sie ein
        Anrecht auf Löschung Ihrer Daten. Von uns gespeicherte Daten werden,
        sollten sie für ihre Zweckbestimmung nicht mehr vonnöten sein und es
        keine gesetzlichen Aufbewahrungsfristen geben, gelöscht. Falls eine
        Löschung nicht durchgeführt werden kann, da die Daten für zulässige
        gesetzliche Zwecke erforderlich sind, erfolgt eine Einschränkung der
        Datenverarbeitung. In diesem Fall werden die Daten gesperrt und nicht
        für andere Zwecke verarbeitet.
      </Typography>

      <Typography variant="h6" gutterBottom>
        Widerspruchsrecht
      </Typography>

      <Typography variant="body1" gutterBottom>
        Nutzer*innen dieser Webseite können von ihrem Widerspruchsrecht Gebrauch
        machen und der Verarbeitung ihrer personenbezogenen Daten zu jeder Zeit
        widersprechen. Wenn Sie eine Berichtigung, Sperrung, Löschung oder
        Auskunft über die zu Ihrer Person gespeicherten personenbezogenen Daten
        wünschen oder Fragen bezüglich der Erhebung, Verarbeitung oder
        Verwendung Ihrer personenbezogenen Daten haben oder erteilte
        Einwilligungen widerrufen möchten, nehmen Sie bitte{' '}
        <Link component={RouterLink} to="/kontakt">
          Kontakt
        </Link>{' '}
        mit uns auf.
      </Typography>
    </Container>
  );
};

export default PrivacyStatement;
