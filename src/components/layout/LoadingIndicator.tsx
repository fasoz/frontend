import React from 'react';
import { CircularProgress, makeStyles } from '@material-ui/core';
import { Typography } from '@material-ui/core/';

const useStyles = makeStyles((theme) => ({
  root: {
    margin: theme.spacing(3, 1, 3, 1),
    textAlign: 'center',
  },
  pulse: {
    animationName: '$pulse',
    animationDuration: '3000ms',
    animationIterationCount: 'infinite',
    animationTimingFunction: 'ease-in-out',
  },
  '@keyframes pulse': {
    '0%': {
      opacity: 0.6,
    },
    '50%': {
      opacity: 0.8,
    },
    '100%': {
      opacity: 0.6,
    },
  },
}));

interface LoadingIndicatorProps {
  text?: string;
}

const LoadingIndicator: React.FC<LoadingIndicatorProps> = ({ text }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <CircularProgress color="secondary" />
      <Typography className={classes.pulse} variant="body2">
        {text ? text : 'Inhalte werden geladen'}
      </Typography>
    </div>
  );
};

export default LoadingIndicator;
