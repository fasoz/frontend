import React from 'react';
import { Chip, makeStyles } from '@material-ui/core';
import BuildIcon from '@material-ui/icons/Build';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.warning.main,
    color: theme.palette.getContrastText(theme.palette.warning.main),
    fontSize: '1.4rem',
  },
}));

const StagingIndicator: React.FC = () => {
  const classes = useStyles();

  return (
    <Chip avatar={<BuildIcon />} className={classes.root} label="Staging" />
  );
};

export default StagingIndicator;
