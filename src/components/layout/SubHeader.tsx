import React, { useContext } from 'react';
import '@fontsource/exo';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Container } from '@material-ui/core';
import UserContext from '../../UserContext';
import MyUserMenu from '../account/MyUserMenu';
import SearchField from './SearchField';
import { Link as RouterLink } from 'react-router-dom';
import AdminMenu from '../admin/AdminMenu';

const useStyles = makeStyles((theme) => ({
  outer: {
    backgroundColor: theme.palette.primary.main,
    boxShadow: '0px 3px 3px grey',
    color: theme.palette.getContrastText(theme.palette.primary.main),
    padding: theme.spacing(1, 0, 1, 0),
    position: 'sticky',
    top: 0,
    zIndex: 100,
  },
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    [theme.breakpoints.down('xs')]: {
      flexFlow: 'column',
    },
  },
  search: {
    flexGrow: 1,
    [theme.breakpoints.down('xs')]: {
      marginBottom: theme.spacing(1),
      width: '100%',
    },
  },
  button: {
    color: theme.palette.getContrastText(theme.palette.primary.main),
    marginLeft: theme.spacing(2),
  },
}));

const SubHeader = (): JSX.Element => {
  const classes = useStyles();

  const { user } = useContext(UserContext);

  return (
    <nav
      className={classes.outer}
      aria-label="Schnellsuche und Kontoverwaltung"
    >
      <Container className={classes.container} maxWidth="lg">
        <div className={classes.search}>
          <SearchField />
        </div>

        <div>
          {user ? (
            <div className={classes.button}>
              <MyUserMenu />
            </div>
          ) : (
            <>
              <Button
                className={classes.button}
                component={RouterLink}
                to="/registrieren"
                variant="outlined"
              >
                Registrieren
              </Button>
              <Button
                className={classes.button}
                component={RouterLink}
                to="/anmelden"
                variant="outlined"
              >
                Anmelden
              </Button>
            </>
          )}
        </div>

        {user?.roles.includes('Administrator') && <AdminMenu />}
      </Container>
    </nav>
  );
};

export default SubHeader;
