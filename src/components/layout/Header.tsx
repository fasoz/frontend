import React from 'react';
import '@fontsource/exo';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Container, Typography } from '@material-ui/core';
import { Link as RouterLink } from 'react-router-dom';
import StagingIndicator from './StagingIndicator';

const useStyles = makeStyles((theme) => ({
  outer: {
    backgroundColor: theme.palette.primary.dark,
    color: theme.palette.getContrastText(theme.palette.primary.dark),
    padding: theme.spacing(1, 0, 1, 0),
  },
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    [theme.breakpoints.down('sm')]: {
      flexFlow: 'column',
    },
  },
  h1: {
    color: theme.palette.getContrastText(theme.palette.primary.dark),
    textDecorationLine: 'unset',
    fontFamily: 'Exo',
    fontSize: '1.75rem',
  },
  navigation: {
    display: 'flex',
    [theme.breakpoints.down('xs')]: {
      flexFlow: 'column',
    },
  },
  link: {
    color: theme.palette.getContrastText(theme.palette.primary.dark),
    fontWeight: 'bold',
  },
}));

const navLinks = [
  { name: 'Nutzungsinformationen', href: '/faq' },
  { name: 'Ethik-Kodex', href: '/ethikkodex' },
  { name: 'Fallverzeichnis', href: '/fallverzeichnis' },
  { name: 'Erweiterte Suche', href: '/erweiterte_suche' },
  { name: 'Kontakt', href: '/kontakt' },
];

const Header = (): JSX.Element => {
  const classes = useStyles();
  return (
    <nav className={classes.outer} aria-label="Hauptnavigation">
      <Container className={classes.container} maxWidth="lg">
        <Typography
          className={classes.h1}
          component={RouterLink}
          to="/"
          variant="h1"
        >
          Fallarchiv Soziale Arbeit
        </Typography>
        {window.location.hostname.startsWith('staging.') && (
          <StagingIndicator />
        )}
        <div className={classes.navigation}>
          {navLinks.map((link) => (
            <Button
              key={link.href}
              component={RouterLink}
              className={classes.link}
              to={link.href}
            >
              {link.name}
            </Button>
          ))}
        </div>
      </Container>
    </nav>
  );
};

export default Header;
