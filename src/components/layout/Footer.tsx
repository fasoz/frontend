import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Link, Typography } from '@material-ui/core';
import { grey } from '@material-ui/core/colors';
import { Link as RouterLink } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  container: {
    color: grey.A200,
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'center',
    padding: theme.spacing(1),
    marginTop: theme.spacing(2),
  },
  item: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    opacity: 0.8,
    whiteSpace: 'nowrap',
  },
}));

const links = [
  { href: '/impressum', text: 'Impressum' },
  { href: '/datenschutzerklärung', text: 'Datenschutzerklärung' },
  { href: '/ethikkodex', text: 'Ethik-Kodex' },
];

const Footer = (): JSX.Element => {
  const classes = useStyles();
  return (
    <Container className={classes.container} component="footer">
      <Typography variant="body2" className={classes.item}>
        Fallarchiv Soziale Arbeit
      </Typography>

      {links.map((link) => (
        <Link
          key={link.text}
          component={RouterLink}
          to={link.href}
          className={classes.item}
        >
          {link.text}
        </Link>
      ))}
    </Container>
  );
};

export default Footer;
