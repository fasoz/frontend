import React, { useState } from 'react';
import { InputBase, Box, Button } from '@material-ui/core';
import { alpha, makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  box: {
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    borderRadius: 5,
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'nowrap',
    padding: 0,
  },
  icon: {
    color: theme.palette.common.white,
    marginLeft: theme.spacing(1),
  },
  input: {
    color: theme.palette.common.white,
    flexGrow: 1,
    padding: theme.spacing(0, 1, 0, 1),
  },
  button: {
    color: theme.palette.common.white,
    margin: theme.spacing(0.25),
  },
}));

const SearchField = (): JSX.Element => {
  const classes = useStyles();
  const history = useHistory();
  const [searchString, setSearchString] = useState<string>('');

  const handleSearch = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    history.push(`/suche/${searchString}`);
    window.scrollTo(0, 0);
  };

  return (
    <form onSubmit={handleSearch} role="search">
      <Box className={classes.box}>
        <SearchIcon className={classes.icon} />
        <InputBase
          className={classes.input}
          placeholder="Fallsuche"
          required
          type="search"
          onChange={(event) => setSearchString(event.currentTarget.value)}
        />
        <Button
          aria-label="search"
          className={classes.button}
          size="small"
          type="submit"
        >
          Suchen
        </Button>
      </Box>
    </form>
  );
};

export default SearchField;
