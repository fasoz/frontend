import React, { useState } from 'react';
import { Formik, Form, Field, FieldAttributes, useField } from 'formik';
import * as yup from 'yup';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  Button,
  Container,
  TextField,
  TextFieldProps,
  Typography,
} from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import userService from '../../services/users';
import { AlertMessage } from '../../types';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      margin: theme.spacing(2, 0, 2, 0),
      textAlign: 'center',
    },
  }),
);

const CustomTextField: React.FC<
  TextFieldProps & FieldAttributes<Record<string, unknown>>
> = ({ label, ...props }) => {
  const [field, meta] = useField<Record<string, unknown>>(props);
  const errorText = meta.error && meta.touched ? meta.error : '';
  return (
    <TextField
      {...field}
      type={props.type}
      label={label}
      fullWidth
      variant="outlined"
      error={Boolean(errorText)}
      helperText={errorText}
    />
  );
};

const validationSchema = yup.object({
  email: yup.string().required('Pflichtfeld').email('Ungültige Email-Adresse'),
});

const ForgotPassword: React.FC = () => {
  const classes = useStyles();

  const [alertMessage, setAlertMessage] = useState<AlertMessage>();

  const initialValues = { email: '' };

  return (
    <Container maxWidth="sm">
      <Typography align="center" gutterBottom variant="h4">
        Passwort vergessen
      </Typography>

      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(true);
          userService
            .forgotPassword(values.email)
            .then(() => {
              setAlertMessage({
                severity: 'success',
                message:
                  'Sie erhalten in Kürze eine Email mit einem Link, der Ihnen erlaubt ein neues Passwort zu setzen.',
              });
            })
            .catch((error) => {
              setAlertMessage({ severity: 'error', message: error });
              setSubmitting(false);
            });
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <Field as={CustomTextField} name="email" label="Email-Adresse" />

            <div className={classes.button}>
              <Button
                color="secondary"
                disabled={isSubmitting}
                size="large"
                type="submit"
                variant="contained"
              >
                Passwort zurücksetzen
              </Button>
            </div>
          </Form>
        )}
      </Formik>

      {alertMessage && (
        <Alert id="alertMessage" severity={alertMessage.severity}>
          {alertMessage.message}
        </Alert>
      )}
    </Container>
  );
};

export default ForgotPassword;
