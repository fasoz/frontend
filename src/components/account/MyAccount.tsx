import React, { useContext } from 'react';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import useSWR from 'swr';

import {
  Button,
  Container,
  Link,
  Table,
  TableBody,
  TableCell,
  TableFooter,
  TableHead,
  TableRow,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Alert, AlertTitle } from '@material-ui/lab';
import EditIcon from '@material-ui/icons/Edit';
import StopIcon from '@material-ui/icons/Stop';

import dayjs from 'dayjs';
import 'dayjs/locale/de';
import relativeTime from 'dayjs/plugin/relativeTime';
import { UAParser } from 'ua-parser-js';

import CaseCard from '../case/Card';
import LoadingIndicator from '../layout/LoadingIndicator';

import UserContext from '../../UserContext';
import usersService from '../../services/users';
import authService from '../../services/auth';
import * as types from '../../types';
import { roleToTranslatedString } from '../../utils/roleTranslation';

dayjs.locale('de');
dayjs.extend(relativeTime);

const useStyles = makeStyles((theme) => ({
  container: {
    marginBottom: theme.spacing(8),
  },
}));

const MyAccount = (): JSX.Element => {
  const classes = useStyles();
  const history = useHistory();

  const { user, setUser } = useContext(UserContext);
  const {
    data: myAccount,
    mutate,
    error,
  } = useSWR(user?.id ? `/api/user/${user.id}` : null, () =>
    user?.id ? usersService.getByUserId(user?.id) : null,
  );

  if (!user)
    return (
      <Container maxWidth="sm">
        <Alert severity="warning">
          <AlertTitle>Sie sind nicht angemeldet.</AlertTitle>
          <Link component={RouterLink} to="/anmelden">
            Klicken Sie hier um zum Anmeldeformular zu gelangen.
          </Link>
        </Alert>
      </Container>
    );

  if (error) {
    return (
      <Container maxWidth="sm">
        <Alert severity="error">
          <AlertTitle>Fehler beim Laden der Kontoinformationen.</AlertTitle>
          {error.toString()}
        </Alert>
      </Container>
    );
  }

  if (!myAccount)
    return <LoadingIndicator text="Ihre Kontoinformationen werden geladen" />;

  const handleRevokeAllSessions = () => {
    if (!window.confirm('Sind sie sicher?')) return;

    usersService
      .editUser(user?.id, { ...myAccount, sessions: [] })
      .then((response) => {
        return Promise.all([mutate(response), authService.logout()]);
      })
      .then(() => {
        setUser(null);
        history.push('/');
      })
      .catch((error) => {
        window.alert(error);
      });
  };

  const uaParser = new UAParser();

  return (
    <>
      <Container className={classes.container} maxWidth="xs">
        <Typography align="center" gutterBottom variant="h4">
          Mein Konto
        </Typography>

        <Table id="myAccountDetails">
          <TableBody>
            <TableRow>
              <TableCell align="right">Email</TableCell>
              <TableCell>{myAccount.username}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell align="right">Name</TableCell>
              <TableCell>{myAccount.name}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell align="right">Rollen</TableCell>
              <TableCell>
                {myAccount.roles
                  .map((role) => roleToTranslatedString(role))
                  .join(', ')}
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell align="center" colSpan={2}>
                <Button
                  component={RouterLink}
                  id="editAccountButton"
                  variant="contained"
                  startIcon={<EditIcon />}
                  to="/meinkonto/bearbeiten"
                >
                  Kontoinformationen bearbeiten
                </Button>
              </TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </Container>

      <Container className={classes.container} maxWidth="sm">
        <Typography align="center" gutterBottom variant="h4">
          Meine Fälle
        </Typography>
        {myAccount.cases.length === 0 ? (
          <Typography align="center" variant="body1">
            keine
          </Typography>
        ) : (
          myAccount.cases.map((thisCase: types.Case) => (
            <CaseCard key={thisCase.id} thisCase={thisCase} />
          ))
        )}
      </Container>

      <Container className={classes.container} maxWidth="md">
        <Typography align="center" gutterBottom variant="h4">
          Meine Sitzungen
        </Typography>

        <Typography align="center" paragraph variant="body1">
          Sie sind mit folgenden Browsern angemeldet
        </Typography>

        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Gerät</TableCell>
              <TableCell>Erste Anmeldung</TableCell>
              <TableCell>Letzte Sitzungsverlängerung</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {myAccount.sessions &&
              myAccount.sessions.map(
                (session: types.UserSession, index: number) => {
                  const userAgent = uaParser
                    .setUA(session.userAgent)
                    .getResult();
                  const loginTimestamp = dayjs(session.loginTimestamp);
                  const lastAccessTimestamp = dayjs(
                    session.lastAccessTimestamp,
                  );

                  return (
                    <TableRow key={index}>
                      <TableCell title={session.userAgent}>
                        {userAgent.browser.name} {userAgent.browser.version},{' '}
                        {userAgent.os.name} {userAgent.os.version}
                      </TableCell>
                      <TableCell
                        title={loginTimestamp.format('DD MMMM YYYY HH:mm:ss')}
                      >
                        {loginTimestamp.fromNow()}
                      </TableCell>
                      <TableCell
                        title={lastAccessTimestamp.format(
                          'DD MMMM YYYY HH:mm:ss',
                        )}
                      >
                        {lastAccessTimestamp.fromNow()}
                      </TableCell>
                    </TableRow>
                  );
                },
              )}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TableCell align="center" colSpan={3}>
                <Button
                  id="deleteAllSessionsButton"
                  variant="contained"
                  startIcon={<StopIcon />}
                  onClick={handleRevokeAllSessions}
                >
                  Auf allen Geräten abmelden.
                </Button>
                <br />
                Erneute Anmeldung erforderlich.
              </TableCell>
            </TableRow>
          </TableFooter>
        </Table>
      </Container>
    </>
  );
};

export default MyAccount;
