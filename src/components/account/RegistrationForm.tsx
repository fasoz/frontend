import React, { useState } from 'react';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import { Field, FieldAttributes, Form, Formik, useField } from 'formik';
import * as yup from 'yup';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  Button,
  Checkbox,
  Container,
  Link,
  TextField,
  TextFieldProps,
  Typography,
} from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import userService from '../../services/users';
import { AlertMessage } from '../../types';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    textField: {
      marginBottom: theme.spacing(2),
      width: '100%',
    },
    checkboxLabel: {
      display: 'flex',
      alignItems: 'center',
    },
    button: {
      margin: theme.spacing(2, 0, 2, 0),
      textAlign: 'center',
    },
  }),
);

const CustomTextField: React.FC<
  TextFieldProps & FieldAttributes<Record<string, unknown>>
> = ({ label, ...props }) => {
  const classes = useStyles();
  const [field, meta] = useField<Record<string, unknown>>(props);
  const errorText = meta.error && meta.touched ? meta.error : '';
  return (
    <TextField
      {...field}
      type={props.type}
      label={label}
      className={classes.textField}
      fullWidth
      variant="outlined"
      error={Boolean(errorText)}
      helperText={errorText}
    />
  );
};

interface SignUpFormValues {
  name: string;
  email: string;
  password: string;
  passwordConfirmation: string;
  privacyConsent: boolean;
  ethicsCodexAccepted: boolean;
}

const initialValues: SignUpFormValues = {
  name: '',
  email: '',
  password: '',
  passwordConfirmation: '',
  privacyConsent: false,
  ethicsCodexAccepted: false,
};

const validationSchema = yup.object({
  name: yup.string(),
  email: yup.string().required('Pflichtfeld').email('Ungültige Email-Adresse'),
  password: yup
    .string()
    .required('Pflichtfeld')
    .min(12, 'Passwort muss mindestens 12 Zeichen lang sein'),
  passwordConfirmation: yup
    .string()
    .required('Pflichtfeld')
    .oneOf([yup.ref('password'), ''], 'Passwörter stimmen nicht überein'),
  privacyConsent: yup.boolean(),
  ethicsCodexAccepted: yup.boolean(),
});

const RegistrationForm: React.FC = () => {
  const classes = useStyles();
  const history = useHistory();

  const [alertMessage, setAlertMessage] = useState<AlertMessage>();

  return (
    <Container maxWidth="sm">
      <Typography align="center" variant="h4" gutterBottom>
        Registrierung
      </Typography>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={async (values, { setSubmitting }) => {
          setSubmitting(true);
          try {
            const newUser = await userService.createUser({
              name: values.name,
              username: values.email,
              password: values.password,
            });
            setAlertMessage({
              severity: 'info',
              message: `Konto "${newUser.username}" wurde erstellt. Sie werden weitergeleitet...`,
            });
            setTimeout(() => {
              history.push('/anmelden');
            }, 5000);
          } catch (error) {
            setAlertMessage({
              severity: 'error',
              message: error.response.data.error || error.response.data,
            });
            setSubmitting(false);
          }
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <Field
              name="name"
              type="text"
              as={CustomTextField}
              label="Vor- und Nachname"
            />

            <Field
              name="email"
              type="email"
              as={CustomTextField}
              label="Email-Adresse"
            />

            <Field
              name="password"
              type="password"
              as={CustomTextField}
              label="Passwort"
            />

            <Field
              name="passwordConfirmation"
              type="password"
              as={CustomTextField}
              label="Passwort wiederholen"
            />

            <Typography align="left" variant="body2" gutterBottom>
              Für Informationen zur Erhebung und Speicherung personenbezogener
              Daten beachten Sie bitte unsere{' '}
              <Link component={RouterLink} to="/datenschutzerklärung">
                Datenschutzerklärung
              </Link>
              . Bitten nehmen Sie auch unseren{' '}
              <Link component={RouterLink} to="/ethikkodex">
                Ethik-Kodex
              </Link>{' '}
              zur Kenntnis.
            </Typography>

            <label className={classes.checkboxLabel}>
              <Field
                required
                component={Checkbox}
                type="checkbox"
                name="privacyConsent"
              />
              Ich bin damit einverstanden, dass meine eingegeben Daten zum
              Zwecke der Erstellung eines Zugangskontos verarbeitet und
              gespeichert werden.
            </label>

            <label className={classes.checkboxLabel}>
              <Field
                required
                component={Checkbox}
                type="checkbox"
                name="ethicsCodexAccepted"
              />
              Ich habe den Ethik-Kodex zur Kenntnis genommen und verpflichte
              mich die ethische Grundhaltung im Umgang mit den Fällen
              einzuhalten.
            </label>

            <div className={classes.button}>
              <Button
                color="secondary"
                size="large"
                type="submit"
                variant="contained"
                disabled={isSubmitting}
              >
                Registrieren
              </Button>
            </div>
          </Form>
        )}
      </Formik>

      {alertMessage && (
        <Alert id="alertMessage" severity={alertMessage.severity}>
          {alertMessage.title && <AlertTitle>{alertMessage.title}</AlertTitle>}
          {alertMessage.message}
        </Alert>
      )}

      <Typography align="center" variant="body1">
        Bereits registriert?{' '}
        <Link component={RouterLink} to="/anmelden">
          Jetzt anmelden
        </Link>
        .
      </Typography>
    </Container>
  );
};

export default RegistrationForm;
