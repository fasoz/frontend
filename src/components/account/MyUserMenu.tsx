import React, { useContext, useState } from 'react';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import { IconButton, Menu, MenuItem } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import authService from '../../services/auth';
import UserContext from '../../UserContext';

const useStyles = makeStyles((theme) => ({
  accountIcon: {
    color: theme.palette.common.white,
    padding: 0,
    marginLeft: theme.spacing(1),
  },
}));

const MyUserMenu = (): JSX.Element => {
  const classes = useStyles();
  const history = useHistory();

  const { user, setUser } = useContext(UserContext);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleLogout = () => {
    void authService.logout();
    setUser(null);
    setAnchorEl(null);
    history.push('/');
  };

  if (!user) return <>Sie nicht angemeldet</>;

  return (
    <>
      Angemeldet als {user.name ? user.name : user.username}
      <IconButton
        className={classes.accountIcon}
        id="accountMenuButton"
        onClick={handleClick}
      >
        <AccountCircleIcon fontSize="large" />
      </IconButton>
      <Menu
        id="accountMenu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        getContentAnchorEl={null}
        onClose={handleClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right',
        }}
      >
        <MenuItem component={RouterLink} to="/meinkonto" onClick={handleClose}>
          Mein Konto
        </MenuItem>
        <MenuItem onClick={handleLogout}>Abmelden</MenuItem>
      </Menu>
    </>
  );
};

export default MyUserMenu;
