import React, { useContext, useState } from 'react';
import { Field, FieldAttributes, Form, Formik, useField } from 'formik';
import * as yup from 'yup';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  Button,
  Checkbox,
  Container,
  Link,
  TextField,
  TextFieldProps,
  Typography,
} from '@material-ui/core';
import { Alert } from '@material-ui/lab';
import UserContext from '../../UserContext';
import authService from '../../services/auth';
import axios from 'axios';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    textField: {
      marginBottom: theme.spacing(2),
      width: '100%',
    },
    checkboxLabel: {
      display: 'inline-block',
      lineHeight: 0,
      marginBottom: theme.spacing(2),
    },
    button: {
      margin: theme.spacing(2, 0, 2, 0),
      textAlign: 'center',
    },
  }),
);

const CustomTextField: React.FC<
  TextFieldProps & FieldAttributes<Record<string, unknown>>
> = ({ label, ...props }) => {
  const classes = useStyles();
  const [field, meta] = useField<Record<string, unknown>>(props);
  const errorText = meta.error && meta.touched ? meta.error : '';
  return (
    <TextField
      {...field}
      type={props.type}
      label={label}
      className={classes.textField}
      fullWidth
      variant="outlined"
      error={Boolean(errorText)}
      helperText={errorText}
    />
  );
};

const validationSchema = yup.object({
  email: yup.string().required('Pflichtfeld').email('Ungültige Email-Adresse'),
  password: yup
    .string()
    .required('Pflichtfeld')
    .min(12, 'Passwort muss mindestens 12 Zeichen lang sein'),
  cookieConsent: yup.boolean(),
});

const LoginForm: React.FC = () => {
  const classes = useStyles();
  const history = useHistory();

  const { user, setUser } = useContext(UserContext);
  const [alertMessage, setAlertMessage] = useState<string>('');

  const initialValues = {
    email: '',
    password: '',
    cookieConsent: false,
  };

  if (user) {
    return (
      <Container maxWidth="sm">
        <Alert severity="info">Sie sind bereits angemeldet.</Alert>
        <Link component={RouterLink} to="/">
          Zurück zur Startseite.
        </Link>
      </Container>
    );
  }

  return (
    <Container maxWidth="sm">
      <Typography align="center" gutterBottom variant="h4">
        Anmeldung
      </Typography>

      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(true);
          authService
            .login(values.email, values.password)
            .then((response) => {
              setUser(response);
              history.push('/');
            })
            .catch((error) => {
              if (
                axios.isAxiosError(error) &&
                typeof error.response?.data?.error === 'string'
              ) {
                setAlertMessage(error.response.data.error as string);
              } else {
                setAlertMessage('Ein unbekannter Fehler ist aufgetreten.');
              }
              setSubmitting(false);
            });
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <Field
              as={CustomTextField}
              name="email"
              label="Email-Adresse"
              type="email"
            />
            <Field
              as={CustomTextField}
              name="password"
              label="Passwort"
              type="password"
            />

            <Typography align="left" variant="body2" gutterBottom>
              Für Informationen zur Erhebung und Speicherung personenbezogener
              Daten beachten Sie bitte unsere{' '}
              <Link component={RouterLink} to="/datenschutzerklärung">
                Datenschutzerklärung
              </Link>
              .
            </Typography>

            <label className={classes.checkboxLabel}>
              <Field
                required
                component={Checkbox}
                type="checkbox"
                name="cookieConsent"
              />
              Ich bin damit einverstanden, dass Anmeldeinformationen in einem
              Cookie auf meinem Computer gespeichert werden.
            </label>

            <div className={classes.button}>
              <Button
                color="secondary"
                disabled={isSubmitting}
                size="large"
                type="submit"
                variant="contained"
              >
                Anmelden
              </Button>
            </div>
          </Form>
        )}
      </Formik>
      {alertMessage !== '' && (
        <Alert id="alertMessage" severity="error">
          {alertMessage}
        </Alert>
      )}

      <Typography align="center" paragraph variant="body1">
        Kein Zugangskonto?{' '}
        <Link component={RouterLink} to="/registrieren">
          Jetzt registrieren
        </Link>
        .
      </Typography>

      <Typography align="center" paragraph variant="body1">
        Password vergessen?{' '}
        <Link component={RouterLink} to="/passwort_vergessen">
          Neues Passwort anfordern
        </Link>
        .
      </Typography>
    </Container>
  );
};

export default LoginForm;
