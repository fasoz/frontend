import React, { useContext } from 'react';
import useSWR from 'swr';
import { Link as RouterLink, useHistory } from 'react-router-dom';
import { useFormik } from 'formik';
import * as yup from 'yup';

import {
  Button,
  Container,
  Link,
  TextField,
  Typography,
} from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import SaveIcon from '@material-ui/icons/Save';

import LoadingIndicator from '../layout/LoadingIndicator';

import UserContext from '../../UserContext';
import usersService from '../../services/users';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    textField: {
      marginBottom: theme.spacing(2),
      width: '100%',
    },
  }),
);

const EditMyAccount: React.FC = () => {
  const classes = useStyles();
  const history = useHistory();
  const { user } = useContext(UserContext);
  const {
    data: myAccount,
    mutate,
    error,
  } = useSWR(user?.id ? `/api/user/${user.id}` : null, () =>
    user?.id ? usersService.getByUserId(user?.id) : null,
  );

  const formik = useFormik({
    initialValues: {
      name: myAccount ? myAccount.name : '',
      username: myAccount ? myAccount.username : '',
      password: '',
      passwordConfirmation: '',
    },
    validationSchema: yup.object({
      name: yup.string(),
      username: yup.string().required().email(),
      password: yup.string().min(12),
      passwordConfirmation: yup
        .string()
        .oneOf(
          [yup.ref('password'), undefined],
          'Passwörter stimmen nicht überein',
        ),
    }),
    onSubmit: async (values) => {
      if (!user) return;
      await usersService.editUser(user.id, {
        name: values.name,
        username: values.username,
        password: values.password !== '' ? values.password : undefined,
      });
      await mutate();
      history.push('/meinkonto');
    },
  });

  if (!user)
    return (
      <Container maxWidth="sm">
        <Alert severity="warning">
          <AlertTitle>Sie sind nicht angemeldet.</AlertTitle>
          <Link component={RouterLink} to="/anmelden">
            Klicken Sie hier um zum Anmeldeformular zu gelangen.
          </Link>
        </Alert>
      </Container>
    );

  if (error) {
    return (
      <Container maxWidth="sm">
        <Alert severity="error">
          <AlertTitle>Fehler beim Laden der Kontoinformationen.</AlertTitle>
          {error.toString()}
        </Alert>
      </Container>
    );
  }

  if (!myAccount)
    return <LoadingIndicator text="Ihre Kontoinformationen werden geladen" />;

  return (
    <Container maxWidth="xs">
      <Typography align="center" gutterBottom variant="h4">
        Kontoinformationen bearbeiten
      </Typography>

      <form onSubmit={formik.handleSubmit}>
        <TextField
          className={classes.textField}
          id="name"
          name="name"
          label="Name"
          variant="outlined"
          value={formik.values.name}
          onChange={formik.handleChange}
          error={formik.touched.name && Boolean(formik.errors.name)}
          helperText={formik.touched.name && formik.errors.name}
        />
        <TextField
          className={classes.textField}
          id="username"
          name="username"
          label="Email"
          variant="outlined"
          value={formik.values.username}
          onChange={formik.handleChange}
          error={formik.touched.username && Boolean(formik.errors.username)}
          helperText={formik.touched.username && formik.errors.username}
        />
        <TextField
          className={classes.textField}
          id="password"
          name="password"
          label="Passwort"
          type="password"
          variant="outlined"
          value={formik.values.password}
          onChange={formik.handleChange}
          error={formik.touched.password && Boolean(formik.errors.password)}
          helperText={formik.touched.password && formik.errors.password}
        />
        <TextField
          className={classes.textField}
          id="passwordConfirmation"
          name="passwordConfirmation"
          label="Passwort bestätigen"
          type="password"
          variant="outlined"
          value={formik.values.passwordConfirmation}
          onChange={formik.handleChange}
          error={
            formik.touched.passwordConfirmation &&
            Boolean(formik.errors.passwordConfirmation)
          }
          helperText={
            formik.touched.passwordConfirmation &&
            formik.errors.passwordConfirmation
          }
        />
        <Button
          color="secondary"
          startIcon={<SaveIcon />}
          variant="contained"
          fullWidth
          type="submit"
        >
          Speichern
        </Button>
      </form>
    </Container>
  );
};

export default EditMyAccount;
