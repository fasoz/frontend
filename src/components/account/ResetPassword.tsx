import React, { useState } from 'react';
import { Field, FieldAttributes, Form, Formik, useField } from 'formik';
import * as yup from 'yup';
import { useHistory, useParams } from 'react-router-dom';
import { makeStyles, Theme } from '@material-ui/core/styles';
import {
  Button,
  Container,
  TextField,
  TextFieldProps,
  Typography,
} from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import userService from '../../services/users';
import { AlertMessage } from '../../types';

const useStyles = makeStyles((theme: Theme) => ({
  textField: {
    marginBottom: theme.spacing(2),
  },
  button: {
    margin: theme.spacing(2, 0, 2, 0),
    textAlign: 'center',
  },
}));

const CustomTextField: React.FC<
  TextFieldProps & FieldAttributes<Record<string, unknown>>
> = ({ label, ...props }) => {
  const classes = useStyles();
  const [field, meta] = useField<Record<string, unknown>>(props);
  const errorText = meta.error && meta.touched ? meta.error : '';
  return (
    <TextField
      {...field}
      type={props.type}
      label={label}
      className={classes.textField}
      fullWidth
      variant="outlined"
      error={Boolean(errorText)}
      helperText={errorText}
    />
  );
};

const validationSchema = yup.object({
  password: yup
    .string()
    .required('Pflichtfeld')
    .min(12, 'Passwort muss mindestens 12 Zeichen lang sein'),
  passwordConfirmation: yup
    .string()
    .required('Pflichtfeld')
    .oneOf([yup.ref('password'), ''], 'Passwörter stimmen nicht überein'),
});

const ResetPassword: React.FC = () => {
  const classes = useStyles();
  const history = useHistory();

  const { code } = useParams<{ code: string }>();
  const [alertMessage, setAlertMessage] = useState<AlertMessage>();

  const initialValues: Record<string, string> = {
    password: '',
    passwordConfirmation: '',
  };

  return (
    <Container maxWidth="sm">
      <Typography align="center" gutterBottom variant="h4">
        Neues Passwort festlegen
      </Typography>

      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(true);
          userService
            .resetPassword(code, values.password)
            .then(() => {
              setAlertMessage({
                severity: 'success',
                title: 'Ihr Passwort wurde geändert',
                message: 'Sie werden zum Anmeldeformular umgeleitet.',
              });
              setTimeout(() => history.push('/anmelden'), 3000);
            })
            .catch((error) => {
              setAlertMessage({
                severity: 'error',
                title: 'Passwort zurücksetzen fehlgeschlagen',
                message: error.message,
              });
              setSubmitting(false);
            });
        }}
      >
        {({ isSubmitting }) => (
          <Form>
            <Field
              name="password"
              type="password"
              as={CustomTextField}
              label="Passwort"
            />

            <Field
              name="passwordConfirmation"
              type="password"
              as={CustomTextField}
              label="Passwort wiederholen"
            />

            <div className={classes.button}>
              <Button
                color="secondary"
                disabled={isSubmitting}
                size="large"
                type="submit"
                variant="contained"
              >
                Passwort festlegen
              </Button>
            </div>
          </Form>
        )}
      </Formik>

      {alertMessage && (
        <Alert id="alertMessage" severity={alertMessage.severity}>
          {alertMessage.title && <AlertTitle>{alertMessage.title}</AlertTitle>}
          {alertMessage.message}
        </Alert>
      )}
    </Container>
  );
};

export default ResetPassword;
