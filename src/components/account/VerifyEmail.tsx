import React, { useEffect, useState } from 'react';
import { Link as RouterLink, useParams } from 'react-router-dom';
import { Container, Link } from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import userService from '../../services/users';
import LoadingIndicator from '../layout/LoadingIndicator';

const VerifyEmail = (): JSX.Element => {
  const { code } = useParams<{ code: string }>();
  const [emailVerified, setEmailVerified] = useState(false);
  const [error, setError] = useState<string>();

  useEffect(() => {
    userService
      .verifyEmailAddress(code)
      .then(() => setEmailVerified(true))
      .catch((error) => {
        if (error instanceof Error) {
          setError(error.message);
        } else {
          setError('Ein unbekannter Fehler ist aufgetreten.');
        }
      });
  }, [code]);

  if (error)
    return (
      <Container maxWidth="sm">
        <Alert severity="error">
          <AlertTitle>Email-Verifizierung fehlgeschlagen</AlertTitle>
          {error}
        </Alert>
      </Container>
    );

  if (emailVerified)
    return (
      <Container maxWidth="sm">
        <Alert severity="success">
          <AlertTitle>Email-Verifizierung erfolgreich</AlertTitle>
          <Link component={RouterLink} to="/anmelden">
            Zur Anmeldung
          </Link>
        </Alert>
      </Container>
    );

  return <LoadingIndicator text="Verifizierung wird abgeschlossen" />;
};

export default VerifyEmail;
