import React, { useState } from 'react';
import {
  Formik,
  Form,
  Field,
  FieldAttributes,
  useField,
  FormikHelpers,
} from 'formik';
import * as yup from 'yup';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import {
  Button,
  Container,
  TextField,
  TextFieldProps,
  Typography,
} from '@material-ui/core';
import { Alert, AlertTitle } from '@material-ui/lab';
import contactService from '../services/contact';
import { AlertMessage, ContactFormMessage } from '../types';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    form: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1),
    },
    textField: {
      borderRadius: 30,
      marginBottom: theme.spacing(2),
      width: '100%',
    },
  }),
);

const CustomTextField: React.FC<
  TextFieldProps & FieldAttributes<Record<string, unknown>>
> = ({ disabled, label, multiline, rows, ...props }) => {
  const classes = useStyles();
  const [field, meta] = useField<Record<string, unknown>>(props);
  const errorText = meta.error && meta.touched ? meta.error : '';
  return (
    <TextField
      {...field}
      disabled={disabled}
      type={props.type}
      label={label}
      className={classes.textField}
      fullWidth
      variant="outlined"
      error={Boolean(errorText)}
      helperText={errorText}
      multiline={multiline}
      rows={rows}
    />
  );
};

const validationSchema = yup.object({
  name: yup.string(),
  email: yup.string().required('Pflichtfeld').email('Ungültige Email-Adresse'),
  subject: yup.string(),
  message: yup.string().required('Pflichtfeld').min(20, 'Nachricht zu kurz'),
});

const ContactForm: React.FC = () => {
  const classes = useStyles();

  const [alertMessage, setAlertMessage] = useState<AlertMessage>();

  const initialValues: ContactFormMessage = {
    name: '',
    email: '',
    subject: '',
    message: '',
  };

  const handleSubmit = (
    values: ContactFormMessage,
    { setSubmitting }: FormikHelpers<ContactFormMessage>,
  ) => {
    setSubmitting(true);
    contactService
      .contact(values)
      .then(() => {
        setAlertMessage({
          severity: 'success',
          title: 'Nachricht erfolgreich übermittelt',
          message: 'Vielen Dank für Ihre Kontaktaufnahme.',
        });
      })
      .catch((error) => {
        setAlertMessage({
          severity: 'error',
          title: 'Nachrichtenübermittlung fehlgeschlagen',
          message: error.response.data.error,
        });
        setSubmitting(false);
      });
  };

  return (
    <Container maxWidth="sm">
      <Typography align="center" variant="h4">
        Kontakt
      </Typography>

      {alertMessage && (
        <Alert id="alertMessage" severity={alertMessage.severity}>
          <AlertTitle>{alertMessage.title}</AlertTitle>
          {alertMessage.message}
        </Alert>
      )}

      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={handleSubmit}
      >
        {({ isSubmitting }) => (
          <Form className={classes.form}>
            <Field
              as={CustomTextField}
              disabled={isSubmitting}
              name="name"
              label="Ihr Name"
            />
            <Field
              as={CustomTextField}
              disabled={isSubmitting}
              name="email"
              label="Ihre Email-Adresse"
            />
            <Field
              as={CustomTextField}
              disabled={isSubmitting}
              name="subject"
              label="Betreff"
            />
            <Field
              as={CustomTextField}
              disabled={isSubmitting}
              name="message"
              label="Ihre Nachricht"
              multiline
              rows={10}
            />

            <div style={{ width: '100%', textAlign: 'center' }}>
              <Button
                color="secondary"
                disabled={isSubmitting}
                size="large"
                type="submit"
                variant="contained"
              >
                Nachricht Abschicken
              </Button>
            </div>
          </Form>
        )}
      </Formik>
    </Container>
  );
};

export default ContactForm;
