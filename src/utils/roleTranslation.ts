import { UserRole } from '../types';

export const roleTranslations = [
  {
    internalName: UserRole.Administrator,
    translatedName: 'Administrator*in',
  },
  {
    internalName: UserRole.Editor,
    translatedName: 'Redakteur*in',
  },
  {
    internalName: UserRole.Researcher,
    translatedName: 'Forschende*r',
  },
  {
    internalName: UserRole.Guest,
    translatedName: 'Gast',
  },
];

/**
 * Translates the given role from its internal name (i.e. the name used in the backend) to German.
 * Returns the internal name if no translation is found.
 * @param role
 */
export const roleToTranslatedString = (role: UserRole): string => {
  const rt = roleTranslations.find((f) => f.internalName === role);
  return rt ? rt.translatedName : role.toString();
};
