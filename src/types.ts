export interface AlertMessage {
  severity: 'error' | 'info' | 'success' | 'warning';
  title?: string;
  message: string;
}

export interface Case {
  id: string;
  title: string;
  description: string;
  fileCategories: Array<string>;
  keywords: Array<string>;
  participants: string;
  pedagogicFields: Array<string>;
  problemAreas: Array<string>;
  published: boolean;
  fileAccessAllowedRoles: Array<UserRole>;
}

export interface CaseFile {
  path: string;
  size: number;
}

export enum CaseFileCategories {
  Komplettakte = 'Komplettakte',
  Primaerdaten = 'Primärdaten',
  Hintergrundinformationen = 'Hintergrundinformationen',
  AufgearbeitetesMaterial = 'Aufgearbeitetes Material',
  Interpretationen = 'Interpretationen',
  Fachliteratur = 'Fachliteratur',
  DidaktischesMaterial = 'Didaktisches Material',
}

export interface CaseSearchHit {
  case: Case;
  highlight: {
    fileCategories?: Array<string>;
    description?: string;
    keywords?: Array<string>;
    participants?: string;
    pedagogicFields?: Array<string>;
    problemAreas?: Array<string>;
    title?: string;
  };
}

export interface CaseSearchNumHits {
  value: number;
  relation: 'eq' | 'gte';
}

export interface CaseSearchResults {
  numHits: CaseSearchNumHits;
  offset: number;
  limit: number;
  hits: Array<CaseSearchHit>;
}

export interface ContactFormMessage {
  name: string;
  email: string;
  subject: string;
  message: string;
}

export interface LoginState {
  id: string;
  name: string;
  roles: Array<string>;
  token: string;
  username: string;
}

export type NewCase = Omit<Case, 'id'>;

export interface User {
  cases: Array<Case>;
  emailVerified: boolean;
  id: string;
  name: string;
  roles: Array<UserRole>;
  username: string;
  sessions?: Array<UserSession>;
}

export interface NewOrEditedUser {
  name: string;
  username: string;
  password?: string;
  roles?: Array<UserRole>;
  sessions?: Array<UserSession>;
}

export enum UserRole {
  Administrator = 'Administrator',
  Editor = 'Editor',
  Researcher = 'Researcher',
  Guest = 'Guest',
}

export interface UserSession {
  lastAccessTimestamp: Date;
  loginTimestamp: Date;
  userAgent: string;
}
