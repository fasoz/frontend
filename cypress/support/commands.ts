// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('createCaseFixtures', () => {
  const user = {
    username: 'temporaryuser@frankfurt-university.de',
    password: 'WYPX9Dg8ua4p',
  };
  cy.request('POST', 'http://localhost:3000/api/testing/create_admin', user);
  cy.request('POST', 'http://localhost:3000/api/auth/login', user).then(
    (response) => {
      const accessToken: string = response.body.token;
      cy.fixture('cases.json').then((cases) => {
        cases.map((newCase) => {
          cy.request({
            method: 'POST',
            url: 'http://localhost:3000/api/case',
            body: newCase,
            headers: { Authorization: `bearer ${accessToken}` },
          });
        });
      });
    },
  );
  cy.request('GET', 'http://localhost:3000/api/auth/logout', user);
});

Cypress.Commands.add(
  'createUserWithRolesAndLogin',
  (roles: Array<string> = []) => {
    const user = {
      name: 'Marshall B. Rosenberg',
      username: 'marshall@example.com',
      password: 'roseBouquet4',
      roles,
    };

    cy.request('POST', '/api/testing/create_admin', user);
    cy.request('POST', '/api/auth/login', {
      username: user.username,
      password: user.password,
    }).then((response) => {
      cy.wrap(response.body.token);
    });
  },
);
