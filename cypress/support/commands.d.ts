// in cypress/support/commands.d.ts
// load type definitions that come with Cypress module
/// <reference types="cypress" />

declare namespace Cypress {
  interface Chainable {
    createCaseFixtures(): void
    createUserWithRolesAndLogin(roles?: Array<string>): Cypress.Chainable<string>
  }
}
