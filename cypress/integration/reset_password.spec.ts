describe('forgot password', () => {
  const testUser = {
    name: 'Grace Hopper',
    username: 'grace@example.org',
    password: 'CODASYLCOBOLCOMTRAN',
  };

  beforeEach(() => {
    cy.clearCookies();
    cy.request('POST', '/api/testing/reset');
    cy.request('POST', '/api/user', testUser);
    cy.request('POST', `/api/testing/verify_email/${testUser.username}`);
  });

  describe('requesting a new password', () => {
    beforeEach(() => {
      cy.visit('/passwort_vergessen');
    });

    it('succeeds for a valid email address', () => {
      cy.get('input[name=email]').type(testUser.username);
      cy.get('button').contains('Passwort zurücksetzen').click();

      cy.get('html').should(
        'contain',
        'Sie erhalten in Kürze eine Email mit einem Link',
      );
    });

    it('looks like it succeeds for an invalid email address', () => {
      cy.get('input[name=email]').type('imNotIntheDatabase@example.com');
      cy.get('button').contains('Passwort zurücksetzen').click();

      cy.get('html').should(
        'contain',
        'Sie erhalten in Kürze eine Email mit einem Link',
      );
    });
  });

  describe('resetting the password', () => {
    beforeEach(() => {
      cy.visit('/passwort_vergessen');
      cy.get('input[name=email]').type(testUser.username);
      cy.get('button').contains('Passwort zurücksetzen').click();

      cy.get('html').should(
        'contain',
        'Sie erhalten in Kürze eine Email mit einem Link',
      );

      cy.request(
        'GET',
        `/api/testing/reset_password_code/${testUser.username}`,
      ).then((response) => {
        cy.visit(`/passwort_zurücksetzen/${response.body.code}`);
      });
    });

    it('succeeds with a valid reset code', () => {
      // set a new password
      cy.get('input[name=password]').type('myNewSecurePassword');
      cy.get('input[name=passwordConfirmation]').type('myNewSecurePassword');
      cy.get('button').contains('Passwort festlegen').click();
      cy.get('html').should('contain', 'Ihr Passwort wurde geändert');
      cy.get('html').should(
        'not.contain',
        'Passwort zurücksetzen fehlgeschlagen',
      );

      // wait for redirect to login form
      cy.url().should('contain', '/anmelden');

      // login
      cy.get('input[name=email]').type(testUser.username);
      cy.get('input[name=password]').type('myNewSecurePassword');
      cy.get('[type="checkbox"]').check();
      cy.get('button').contains('Anmelden').click();

      // verify that the login worked
      cy.get('#root').should('contain', `Angemeldet als ${testUser.name}`);
    });

    it('fails with an invalid reset code', () => {
      cy.visit(
        `/passwort_zurücksetzen/CFjVKvDuVef6s4CxW5HfzbwN77EP8wNrrNaCe5Q5H8EdImpc`,
      );
      cy.get('input[name=password]').type('myNewSecurePassword');
      cy.get('input[name=passwordConfirmation]').type('myNewSecurePassword');
      cy.get('button').contains('Passwort festlegen').click();
      cy.get('html').should('contain', 'Passwort zurücksetzen fehlgeschlagen');
      cy.get('html').should('not.contain', 'Ihr Passwort wurde geändert');
    });

    it("fails with a valid reset code but a password that's too short", () => {
      cy.get('input[name=password]').type('eyShorty');
      cy.get('input[name=passwordConfirmation]').type('eyShorty');
      cy.get('button').contains('Passwort festlegen').click();
      cy.get('html').should(
        'contain',
        'Passwort muss mindestens 12 Zeichen lang sein',
      );
    });
  });
});
