describe('login', () => {
  const testUser = {
    name: 'John Doe',
    username: 'john.doe@example.com',
    password: 'D3d3R4aMUJXA',
  };

  beforeEach(() => {
    cy.request('POST', '/api/testing/reset');
    cy.request('POST', '/api/user', testUser);
    cy.request('POST', `/api/testing/verify_email/${testUser.username}`);
    cy.visit('/anmelden');
  });

  it('fails without checking the cookie consent', () => {
    cy.get('input[name=email]').type(testUser.username);
    cy.get('input[name=password]').type(testUser.password);
    cy.get('button').contains('Anmelden').click();
    cy.url().should('include', 'anmelden');
  });

  it('succeeds with valid login', () => {
    cy.get('input[name=email]').type(testUser.username);
    cy.get('input[name=password]').type(testUser.password);
    cy.get('[type="checkbox"]').check();
    cy.get('button').contains('Anmelden').click();
    cy.get('#root').should('contain', `Angemeldet als ${testUser.name}`);
  });

  it('fails when empty form is submitted', () => {
    cy.get('[type="checkbox"]').check();
    cy.get('button').contains('Anmelden').click();
    cy.get('div')
      .contains('Email-Adresse')
      .parent()
      .should('contain', 'Pflichtfeld');
    cy.get('div')
      .contains('Passwort')
      .parent()
      .should('contain', 'Pflichtfeld');
  });

  it('fails with invalid login', () => {
    cy.get('input[name=email]').type('sneaky@example.org');
    cy.get('input[name=password]').type('6JXva3jLMLJV');
    cy.get('[type="checkbox"]').check();
    cy.get('button').contains('Anmelden').click();
    cy.get('#alertMessage').should('contain', 'invalid username or password');
  });

  it('fails with valid email and incorrect password', () => {
    cy.get('input[name=email]').type(testUser.username);
    cy.get('input[name=password]').type('MKni7BdDsXry');
    cy.get('[type="checkbox"]').check();
    cy.get('button').contains('Anmelden').click();
    cy.get('#alertMessage').should('contain', 'invalid username or password');
  });

  it('fails with valid email and empty password', () => {
    cy.get('input[name=email]').type(testUser.username);
    cy.get('[type="checkbox"]').check();
    cy.get('button').contains('Anmelden').click();
    cy.get('div')
      .contains('Passwort')
      .parent()
      .should('contain', 'Pflichtfeld');
  });

  it('has a link to the registration form', () => {
    cy.get('a')
      .contains('Jetzt registrieren')
      .should('have.attr', 'href', '/registrieren');
  });
});
