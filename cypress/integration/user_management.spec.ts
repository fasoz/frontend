describe('user management', () => {
  beforeEach(() => {
    cy.request('POST', '/api/testing/reset');
  });

  it('is inaccessible when not logged in', () => {
    cy.visit('/zugangsverwaltung');
    cy.get('#root').should('contain', 'Zugriff verweigert');
    cy.get('#root').should('contain', 'Sie sind nicht angemeldet');
  });

  it('is inaccessible when logged in but user lacks necessary permissions', () => {
    const user = {
      username: 'marshall@example.com',
      password: 'roseBouquet4',
      roles: [],
    };
    cy.request('POST', '/api/testing/create_admin', user);
    cy.request('POST', '/api/auth/login', {
      username: user.username,
      password: user.password,
    });

    cy.visit('/zugangsverwaltung');
    cy.get('#root').should('contain', 'Zugriff verweigert');
    cy.get('#root').should(
      'contain',
      'Ihnen fehlen die erforderlichen Berechtigungen',
    );
  });

  describe('when logged in as administrator', () => {
    beforeEach(() => {
      cy.createUserWithRolesAndLogin(['Administrator']);
      cy.visit('/zugangsverwaltung');
    });

    it('the list of users contains user who is logged in', () => {
      cy.get('#usersTable').should('contain', 'marshall@example.com');
    });

    it('adding a user succeeds', () => {
      cy.get('#addUserButton').click();

      cy.get('input[name=name]').type('Immanuel Kant');
      cy.get('input[name=username]').type('manny@example.org');
      cy.get('input[name=password]').type('AkzeptableEntropie');
      cy.get('input[name=passwordConfirmation]').type('AkzeptableEntropie');
      cy.get('[type="checkbox"]').check('Researcher');
      cy.get('button').contains('Hinzufügen').click();

      cy.get('#usersTable').get('tbody').should('contain', 'Immanuel Kant');
    });

    describe('an existing user', () => {
      beforeEach(() => {
        cy.request('POST', '/api/testing/create_admin', {
          name: 'René Descartes',
          username: 'rene@example.org',
          password: 'cogito ergo sum',
          passwordConfirmation: 'cogito ergo sum',
        });
        cy.visit('/zugangsverwaltung');
      });

      it('can be deleted', () => {
        cy.get('#usersTable')
          .contains('tr', 'Descartes')
          .within((_tr) => {
            cy.get('.deleteUserButton').click();
          });

        cy.get('#usersTable').should('not.contain', 'Descartes');
        cy.reload();
        cy.get('#usersTable').should('not.contain', 'Descartes');
      });

      it('can be edited', () => {
        cy.get('#usersTable')
          .contains('tr', 'Descartes')
          .within((_tr) => {
            cy.get('.editUserButton').click();
          });

        cy.get('input[name=name]').clear().type('Ephemeral Philosopher');
        cy.get('input[name=username]').clear().type('noreply@example.org');
        cy.get('input[name=password]').type('I am no longer');
        cy.get('input[name=passwordConfirmation]').type('I am no longer');
        cy.get('[type="checkbox"]').uncheck('Researcher');
        cy.get('[type="checkbox"]').uncheck('Administrator');
        cy.get('[type="checkbox"]').uncheck('Editor');
        cy.get('[type="checkbox"]').check('Guest');
        cy.get('button').contains('Speichern').click();

        cy.get('#usersTable').should('not.contain', 'Descartes');
        cy.get('#usersTable').should('contain', 'Ephemeral Philosopher');
      });
    });
  });
});
