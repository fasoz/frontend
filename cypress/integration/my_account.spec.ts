describe('my account', () => {
  beforeEach(() => {
    cy.request('POST', '/api/testing/reset');
    cy.createUserWithRolesAndLogin(['Administrator']);
  });

  it('is reachable via the top navigation bar', () => {
    cy.visit('/');
    cy.get('button#accountMenuButton').click();
    cy.get('#accountMenu').contains('Mein Konto').click();

    cy.get('#root').should('contain', 'Kontoinformationen bearbeiten');
    cy.get('#root').should('contain', 'Meine Fälle');
    cy.get('#root').should('contain', 'Meine Sitzungen');
  });

  describe('lets you change your', () => {
    beforeEach(() => {
      cy.visit('/meinkonto');
      cy.get('#editAccountButton').click();
      cy.get('#root').should('contain', 'Kontoinformationen bearbeiten');
    });

    it('display name', () => {
      cy.get('input[name=name]').clear().type('Immanuel Kant');
      cy.get('button').contains('Speichern').click();

      cy.get('table#myAccountDetails')
        .contains('Name')
        .parent()
        .should('contain', 'Immanuel Kant');
    });

    it('email address', () => {
      cy.get('input[name=username]').clear().type('manny@example.org');
      cy.get('button').contains('Speichern').click();

      cy.get('table#myAccountDetails')
        .contains('Email')
        .parent()
        .should('contain', 'manny@example.org');
    });

    it('password', () => {
      // change password
      const newPassword = 'volumetric rendering is expensive';
      cy.get('input[name=password]').clear().type(newPassword);
      cy.get('input[name=passwordConfirmation]').clear().type(newPassword);
      cy.get('button').contains('Speichern').click();

      // log out
      cy.get('button#accountMenuButton').click();
      cy.get('#accountMenu').contains('Abmelden').click();

      // log back in with new password
      cy.get('a')
        .contains('Klicken Sie hier um zum Anmeldeformular zu gelangen.')
        .click();
      cy.get('input[name=email]').type('marshall@example.com');
      cy.get('input[name=password]').type(newPassword);
      cy.get('[type="checkbox"]').check();
      cy.get('button').contains('Anmelden').click();

      // observe log in
      cy.get('#root').should('contain', 'Angemeldet als Marshall B. Rosenberg');
    });
  });

  it('logs you out when you delete all sessions', () => {
    cy.visit('/meinkonto');
    cy.get('button#deleteAllSessionsButton').click();

    cy.get('#root').should(
      'not.contain',
      'Angemeldet als Marshall B. Rosenberg',
    );
    cy.get('#root').should('contain', 'Registrieren');
    cy.get('#root').should('contain', 'Anmelden');
  });
});
