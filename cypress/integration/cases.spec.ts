// eslint-disable-next-line @typescript-eslint/triple-slash-reference
/// <reference path="../support/commands.d.ts" />
import 'cypress-file-upload';

beforeEach(() => {
  cy.request('POST', '/api/testing/reset');
});

describe('when not logged in', () => {
  it('there is no button for adding a case', () => {
    cy.visit('/fallverzeichnis');
    cy.get('#addCaseButton').should('not.exist');
  });

  describe('case listing', () => {
    beforeEach(() => {
      cy.createCaseFixtures();
      cy.visit('/fallverzeichnis');
    });

    it('displays the correct number of cases', () => {
      cy.fixture('cases.json').then((cases) => {
        cy.get('.caseCard').should('have.length', 3);
      });
    });

    it('displays basic information', () => {
      cy.get('h5').contains('Hermes').parent().parent().as('caseListing');
      cy.get('@caseListing').should('contain', 'Do Re Mi, Fa So');
      cy.get('@caseListing').should('contain', 'Lightning fast coachman.');
    });

    it('has clickable title', () => {
      cy.contains('Hermes').click();
      cy.url().should('match', /^http:\/\/localhost:3000\/fall\/[a-zA-Z0-9]+$/);
    });
  });

  describe('case view', () => {
    beforeEach(() => {
      cy.createCaseFixtures();
      cy.request({
        method: 'GET',
        url: '/api/case',
      }).then((response) => {
        cy.visit(`/fall/${response.body[0].id}/`);
      });
    });

    it('does not have a file listing', () => {
      cy.get('#caseFiles').should('not.exist');
      cy.contains('Fallmaterial')
        .parent()
        .should('contain', 'Bitte melden Sie sich an');
    });

    it("doesn't have an edit button", () => {
      cy.get('#editCase').should('not.exist');
      cy.get('html').should('not.contain', 'Fall bearbeiten');
    });

    it("doesn't have a delete button", () => {
      cy.get('#deleteCase').should('not.exist');
      cy.get('html').should('not.contain', 'Fall wurde gelöscht');
    });
  });
});

describe('when logged in', () => {
  let accessToken: string;

  beforeEach(() => {
    cy.createUserWithRolesAndLogin([
      'Administrator',
      'Editor',
      'Researcher',
    ]).then((token: string) => (accessToken = token));
  });

  it('there is a button for adding a case', () => {
    cy.visit('/fallverzeichnis');
    cy.get('#addCaseButton')
      .should('exist')
      .should('contain', 'Fall hinzufügen');
  });

  describe('adding a case', () => {
    beforeEach(() => {
      cy.visit('/fall/neu');
    });

    it('succeeds with just the title', () => {
      cy.get('input[name=title]').type('Bobby');
      cy.get('#buttonSaveCase').click();

      cy.get('h2').should('contain', 'Bobby');
    });

    it('succeeds with all fields filled out', () => {
      cy.get('input[name=title]').type('Helena');
      cy.get('textarea[name=description]').type(
        'Lorem ipsum dolor sit amet.\nAt vero eos et accusam et justo duo dolores et ea rebum.',
      );
      cy.get('#fileCategoriesField').type('Interview{enter}Biografie{enter}');
      cy.get('#keywordsField').type('Eins{enter}Zwei{enter}Drei{enter}');
      cy.get('textarea[name=participants]').type('Person A, Institution B');
      cy.get('#pedagogicFieldsField').type('Field 1{enter}Field A{enter}');
      cy.get('#problemAreasField').type('Area A{enter}Area B{enter}');
      cy.get('input[type="checkbox"]#published').click();
      cy.get('#buttonSaveCase').click();

      cy.get('h2')
        .should('contain', 'Helena')
        .parent()
        .parent()
        .as('caseListing');

      cy.get('@caseListing')
        .should('contain', 'Schlagwörter')
        .as('schlagwörter');
      cy.get('@schlagwörter').contains('Eins');
      cy.get('@schlagwörter').contains('Zwei');
      cy.get('@schlagwörter').contains('Drei');

      cy.get('@caseListing').should('contain', 'Beteiligte').as('beteiligte');
      cy.get('@beteiligte').contains('Person A');
      cy.get('@beteiligte').contains('Institution B');

      cy.get('@caseListing')
        .should('contain', 'Institution und Arbeitsfeld')
        .as('problemfeld');
      cy.get('@problemfeld').contains('Area A');
      cy.get('@problemfeld').contains('Area B');

      cy.get('@caseListing')
        .should('contain', 'Pädagogisches Handlungsfeld / Arbeitsfeld')
        .as('handlungsfeld');
      cy.get('@handlungsfeld').contains('Field 1');
      cy.get('@handlungsfeld').contains('Field A');

      cy.get('@caseListing').contains('Lorem ipsum dolor sit amet');
    });

    it('displays it in the case listing', () => {
      cy.get('input[name=title]').type('Bobby');
      cy.get('input[name=published]').click();
      cy.get('#buttonSaveCase').click();

      cy.visit('/fallverzeichnis');
      cy.get('.caseCard').should('have.length', 1);
      cy.get('h5').contains('Bobby');
    });
  });

  describe('editing a case', () => {
    beforeEach(() => {
      cy.fixture('cases.json').then((cases) => {
        cy.request({
          method: 'POST',
          url: '/api/case',
          body: cases[0],
          headers: { Authorization: `bearer ${accessToken}` },
        }).then((response) => {
          cy.visit(`/fall/${response.body.id}/bearbeiten`);
        });
      });
    });

    it('succeeds when all fields changed', () => {
      cy.get('input[name=title]').clear().type('New Title');
      cy.get('textarea[name=description]').clear().type('All new description');
      cy.get('#fileCategoriesField')
        .clear()
        .type('{backspace}{backspace}La{enter}Le{enter}Lu{enter}');
      cy.get('#keywordsField')
        .clear()
        .type('{backspace}{backspace}{backspace}Bla{enter}Blubb{enter}');
      cy.get('textarea[name=participants]').clear().type('You, Me');
      cy.get('#buttonSaveCase').click();

      cy.contains('New Title');

      cy.get('h6').get('#caseKeywords').as('keywords');
      cy.get('@keywords').children().should('have.length', 2);
      cy.get('@keywords').find('div').eq(0).should('contain', 'Bla');
      cy.get('@keywords').find('div').eq(1).should('contain', 'Blubb');

      cy.get('h6').contains('Materialarten').parent().as('fileCategories');
      cy.get('@fileCategories').should('contain', 'La');
      cy.get('@fileCategories').should('contain', 'Le');
      cy.get('@fileCategories').should('contain', 'Lu');

      cy.get('h6').contains('Beteiligte').parent().as('participants');
      cy.get('@participants').should('contain', 'You');
      cy.get('@participants').should('contain', 'Me');

      cy.get('h6')
        .get('#caseDescription')
        .should('contain', 'All new description');
    });

    it('shows error when title is removed', () => {
      cy.get('input[name=title]').clear().blur();
      cy.get('div').contains('Titel').parent().should('contain', 'Pflichtfeld');
    });

    it('file uploads succeed', () => {
      // upload file
      cy.get('#caseFiles')
        .get('#Hintergrundinformationen')
        .find('input[type="file"]')
        .attachFile('example.pdf');
      cy.get('#buttonSaveCase').click();

      // check if file is listed in the case
      cy.get('#caseFiles').should('contain', 'example.pdf');
    });

    it('file deletions succeed', () => {
      // upload file
      cy.get('#caseFiles')
        .get('#Hintergrundinformationen')
        .find('input[type="file"]')
        .attachFile('example.pdf');

      // delete file
      cy.get('#caseFiles')
        .get('#Hintergrundinformationen')
        .get('.buttonDelete')
        .click();
      cy.get('#buttonSaveCase').click();

      // check if file is listed in the case
      cy.get('#caseFiles').should('not.contain', 'example.pdf');
    });
  });

  describe('case view', () => {
    let expectedCase;

    beforeEach(() => {
      cy.fixture('cases.json').then((cases) => {
        cy.request({
          method: 'POST',
          url: '/api/case',
          body: cases[0],
          headers: { Authorization: `bearer ${accessToken}` },
        }).then((response) => {
          expectedCase = response.body;
          cy.visit(`/fall/${response.body.id}`);
        });
      });
    });

    it('displays all information', () => {
      cy.contains('Helena');

      cy.get('h6').get('#caseKeywords').as('keywords');
      cy.get('@keywords').children().should('have.length', 3);
      cy.get('@keywords')
        .find('div')
        .eq(0)
        .should('contain', expectedCase.keywords[0]);
      cy.get('@keywords')
        .find('div')
        .eq(1)
        .should('contain', expectedCase.keywords[1]);
      cy.get('@keywords')
        .find('div')
        .eq(2)
        .should('contain', expectedCase.keywords[2]);

      cy.get('h6').contains('Materialarten').parent().as('fileCategories');
      cy.get('@fileCategories').should(
        'contain',
        expectedCase.fileCategories[0],
      );
      cy.get('@fileCategories').should(
        'contain',
        expectedCase.fileCategories[1],
      );
      cy.get('@fileCategories').should(
        'contain',
        expectedCase.fileCategories[2],
      );

      cy.get('h6').contains('Beteiligte').parent().as('participants');
      cy.get('@participants').should('contain', expectedCase.participants[0]);
      cy.get('@participants').should('contain', expectedCase.participants[1]);
      cy.get('@participants').should('contain', expectedCase.participants[2]);

      cy.get('h6')
        .get('#caseDescription')
        .should('contain', expectedCase.description);
    });

    it('has a working delete button', () => {
      cy.get('#deleteCase').click();
      cy.get('html').should('contain', 'Fall wurde gelöscht');
    });

    it('has a working edit button', () => {
      cy.get('#editCase').click();
      cy.get('html').should('contain', 'Metadaten bearbeiten');
    });

    it('does have a file listing', () => {
      cy.get('#caseFiles').should('exist');
      cy.contains('Fallmaterial')
        .parent()
        .should('not.contain', 'Bitte melden Sie sich an');
    });
  });
});
