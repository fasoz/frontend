describe('registration', () => {
  beforeEach(() => {
    cy.request('POST', '/api/testing/reset');
    cy.visit('/registrieren');
  });

  it('succeeds when all fields are filled out and password is long enough', () => {
    cy.get('input[name=name]').type('Bob Ross');
    cy.get('input[name=email]').type('bob.ross@example.org');
    cy.get('input[name=password]').type('longAndSecurePassword123');
    cy.get('input[name=passwordConfirmation]').type('longAndSecurePassword123');
    cy.get('[type="checkbox"]').check();
    cy.get('button').contains('Registrieren').click();
    cy.get('#alertMessage').should(
      'contain',
      'Konto "bob.ross@example.org" wurde erstellt. Sie werden weitergeleitet...',
    );
  });

  it('fails when empty form is submitted', () => {
    cy.get('[type="checkbox"]').check();
    cy.get('button').contains('Registrieren').click();
    cy.get('div')
      .contains('Email-Adresse')
      .parent()
      .should('contain', 'Pflichtfeld');
    cy.get('div')
      .contains('Passwort')
      .parent()
      .should('contain', 'Pflichtfeld');
    cy.get('div')
      .contains('Passwort wiederholen')
      .parent()
      .should('contain', 'Pflichtfeld');
  });

  it('succeeds when only email and password are filled out', () => {
    cy.get('input[name=email]').type('alice.wonder@example.org');
    cy.get('input[name=password]').type('longAndSecurePassword123');
    cy.get('input[name=passwordConfirmation]').type('longAndSecurePassword123');
    cy.get('[type="checkbox"]').check();
    cy.get('button').contains('Registrieren').click();
    cy.get('#alertMessage').should(
      'contain',
      'Konto "alice.wonder@example.org" wurde erstellt. Sie werden weitergeleitet...',
    );
  });

  it('shows error when invalid email address is provided', () => {
    cy.get('input[name=email]').type('bobbyTables').blur();
    cy.get('div')
      .contains('Email-Adresse')
      .parent()
      .should('contain', 'Ungültige Email-Adresse');
  });

  it('shows error when passwords do not match', () => {
    cy.get('input[name=password]').type('longAndSecurePassword123');
    cy.get('input[name=passwordConfirmation]')
      .type('longAndSecurePassword321')
      .blur();
    cy.get('div')
      .contains('Passwort wiederholen')
      .parent()
      .should('contain', 'Passwörter stimmen nicht überein');
  });

  it('shows error when password is not repeated', () => {
    cy.get('input[name=email]').type('bob.ross@example.org');
    cy.get('input[name=password]').type('longAndSecurePassword123');
    cy.get('[type="checkbox"]').check();
    cy.get('button').contains('Registrieren').click();
    cy.get('div')
      .contains('Passwort wiederholen')
      .parent()
      .should('contain', 'Pflicht');
  });

  it('shows error when password is too short', () => {
    cy.get('input[name=password]').type('abcdabcdabc');
    cy.get('input[name=passwordConfirmation]').type('abcdabcdabc').blur();

    cy.get('div')
      .contains('Passwort')
      .parent()
      .should('contain', 'Passwort muss mindestens 12 Zeichen lang sein');
  });

  it('fails with a username that already exists', () => {
    cy.request('POST', '/api/user', {
      username: 'someone@example.com',
      password: '3qwe12tyu2sdcC',
    });

    cy.get('input[name=email]').type('someone@example.com');
    cy.get('input[name=password]').type('1234567890234567890');
    cy.get('input[name=passwordConfirmation]').type('1234567890234567890');
    cy.get('[type="checkbox"]').check();
    cy.get('button').contains('Registrieren').click();
    cy.get('#alertMessage').should('contain', 'Username already exists');
  });

  it("fails when privacy consent checkbox isn't checked", () => {
    cy.get('input[name=name]').type('Bob Ross');
    cy.get('input[name=email]').type('bob.ross@example.org');
    cy.get('input[name=password]').type('longAndSecurePassword123');
    cy.get('input[name=passwordConfirmation]').type('longAndSecurePassword123');
    cy.get('button').contains('Registrieren').click();
    cy.get('#alertMessage').should('not.exist');
  });

  it('has a link to the login form', () => {
    cy.get('a')
      .contains('Jetzt anmelden')
      .should('have.attr', 'href', '/anmelden');
  });
});
