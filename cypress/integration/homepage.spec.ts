describe('Homepage', () => {
  it('displays the full site name', function () {
    cy.visit('/');
    cy.contains('Fallarchiv Soziale Arbeit');
  });
});
